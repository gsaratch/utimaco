/**************************************************************************************************
 *
 * Filename           : exception.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Exception Object
 *
 *************************************************************************************************/
#ifndef __EXCEPTION_H
#define __EXCEPTION_H

#include "cxi.h"

namespace cxi
{
  class CXIAPI Exception
  {
    public:      
      int  err;       //!< error code
      char *err_str;  //!< plain text error message
      char *where;    //!< name of function where error occurs
      int  line;      //!< line number in source code
      
      Exception(const char *where, int err);
      Exception(const char *where, int line, int err);
      
      Exception(const char *where, const char *err_str);
      Exception(const char *where, int line, const char *err_str);
      Exception(const char *where, int line, int err, const char *err_str);
      
      virtual ~Exception(void);
      
      static void throwMe();
  };   
}

#endif
