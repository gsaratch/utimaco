/**************************************************************************************************
 *
 * Filename           : item.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Parse / Construct command items and property lists
 *
 *************************************************************************************************/
#ifndef __ITEM_H
#define __ITEM_H

#include "cxi.h"

namespace cxi
{  
  class CXIAPI Item : public ByteArray
  {        
    public:
      Item(void);
      Item(const ByteArray &data);
      Item(char *p_data, int l_data);
      Item(const char tag[], const ByteArray &value);
      virtual ~Item(void);

      ByteArray getValue(void);
      Item find(const char tag[]);

      // static members used to parse item lists
      static Item find(const char tag[], unsigned char *p_data, unsigned int l_data, int idx = 0);
      static Item find(const char tag[], const ByteArray &data, int idx = 0);
      
      static ByteArray findValue(const char tag[], unsigned char *p_data, unsigned int l_data, int idx = 0);      
      static ByteArray findValue(const char tag[], const ByteArray &data, int idx = 0);
  };
}

#endif
