/**************************************************************************************************
 *
 * Filename           : blob.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Key Blob Object
 *
 *************************************************************************************************/
#ifndef __KEYBLOB_H
#define __KEYBLOB_H

#include "cxi.h"

namespace cxi
{  
  class CXIAPI KeyBlob : public ByteArray
  {    
    public:
      KeyBlob(void);
      KeyBlob(ByteArray b);
      KeyBlob(char *data, int len);
      virtual ~KeyBlob(void);

      // set functions
      void setDES(char *data, int len);
      void setAES(char *data, int len);

      void setRSA
      (
        char *mod, int l_mod,
        char *pub, int l_pub,  
        char *p, int l_p,
        char *q, int l_q,
        char *dp, int l_dp,
        char *dq, int l_dq,
        char *u, int l_u,
        char *prv, int l_prv
      );
      
      void setRSA
      (
        char *mod, int l_mod,
        char *pub, int l_pub
      );

      void setEC
      (
        char *dp, int l_dp,
        char *pub, int l_pub,
        char *prv, int l_prv
      );

      void setDSA
      (
        char *p, int l_p,
        char *q, int l_q,
        char *g, int l_g,
        char *pub, int l_pub,
        char *prv, int l_prv
      );

      // get functions
      ByteArray getProperty(int property);
      ByteArray getKeyComp(char tag[]);
      ByteArray getPublic(void);      
      ByteArray getPrivate(void);      
      ByteArray getSecret(void);      

  };
  
  // backward compatibility
  #define Blob KeyBlob
}

#endif
