/**************************************************************************************************
 *
 * Filename           : keylist.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Encapsulates array of Key Objects
 *
 *************************************************************************************************/
#ifndef __KEYLIST_H
#define __KEYLIST_H

#ifdef WIN32
  #pragma warning (disable: 4251)
#endif

#include <vector>

#include "cxi.h"

namespace cxi
{ 
  class CXIAPI KeyList
  {
    private:
      std::vector<PropertyList> list;

    public:
      KeyList(void);
      virtual ~KeyList(void);

      int size(void);
      void add(const PropertyList &pl);
      void sort(void);      

      PropertyList &operator[](int i);      
  };    
}

#endif
