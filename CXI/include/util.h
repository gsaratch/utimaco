/**************************************************************************************************
 *
 * Filename           : util.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Utility Functions
 *
 *************************************************************************************************/
#ifndef __UTIL_H
#define __UTIL_H

#include "cxi.h"

/******************************************************************************
 *
 * Macros
 *
 ******************************************************************************/
#ifndef MIN
#define MIN(a,b)   ((a)<(b)?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b)   ((a)>(b)?(a):(b))
#endif

namespace cxi
{
  class CXIAPI Util
  {
    public:
      static void store_int2(unsigned int val, unsigned char *buf);
      static void store_int3(unsigned int val, unsigned char *buf);
      static void store_int4(unsigned int val, unsigned char *buf);
      
      static unsigned int load_int2(unsigned char *buf);
      static unsigned int load_int3(unsigned char *buf);
      static unsigned int load_int4(unsigned char *buf);
      
      static char *rtrim(char *s, char c);
      
      static void xtrace(const char *text, void *data, int len);
      static void xtrace(const char *text, const ByteArray &ba);            
  };   
}

#endif
