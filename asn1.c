#include "asn1.h"
void encode(unsigned char *data,int inLength, unsigned char tagValue,struct TLV *tlv){
	unsigned char firstByte;
	unsigned char secondByte=0x80;
	unsigned int numberOfBytes=0;
	int i;
	bool flag=true;
	tlv->tag=tagValue;
	if(inLength<0x7F){
		//numberOfBytes++;
		flag=false;
	}
	else{
		i=inLength;
		while(i!=0){
			i/=256;
			numberOfBytes++;
		}
		secondByte=0x80|numberOfBytes;
	}
	
	if(flag==false){
		tlv->length=1;
		tlv->lengthBytes=(unsigned char*) malloc(sizeof(unsigned char));
		tlv->lengthBytes[0]=(unsigned char)inLength;

	}
	else{
		tlv->length=numberOfBytes+1;
		tlv->lengthBytes=(unsigned char*) malloc(sizeof(unsigned char)*(numberOfBytes+1));
		tlv->lengthBytes[0]=secondByte;
		i=inLength;
		for(int j=0;j<numberOfBytes;j++)
		{
			tlv->lengthBytes[numberOfBytes-j]=(unsigned char)i%256;
			i/=256;
		}	
	}
	tlv->valueLength=inLength;
	tlv->value=(unsigned char*)malloc(sizeof(unsigned char)*inLength);
	for(i=0;i<inLength;i++){
		tlv->value[i]=data[i];
	}
}



void display(struct TLV *tlv){
	int i;
	if(tlv){
	printf("\n%02x ",tlv->tag);
	for(i=0;i<tlv->length;i++){
	printf("%02x ",tlv->lengthBytes[i]);
	}
	for(i=0;i<tlv->valueLength;i++){
	printf("%02x ",tlv->value[i]);
	}
	printf("\n");
	}
}
void tlvToStream(unsigned char *data, int length, struct TLV *tlv){
	
	int i,count=0;
	data[count++]=tlv->tag;
	for(i=0;i<tlv->length;i++){
		data[count++]=tlv->lengthBytes[i];
	}
	for(i=0;i<tlv->valueLength;i++){
		data[count++]=tlv->value[i];
	}
}

void tlvToStream1(unsigned char **data, int *length, struct TLV *tlv){
	
	int i,count=0;
	*length=tlv->length+tlv->valueLength+1;
	if(*data)
		free(*data);

	*data=(unsigned char*)malloc(sizeof(unsigned char)*(*length));
	*(*data+count++)=tlv->tag;
	//data[count++]=tlv->tag;
	for(i=0;i<tlv->length;i++){
		//data[count++]=tlv->lengthBytes[i];
		*(*data+count++)=tlv->lengthBytes[i];
	}
	for(i=0;i<tlv->valueLength;i++){
		//data[count++]=tlv->value[i];
		*(*data+count++)=tlv->value[i];
	}
}



void version(struct TLV *tlv)
{
	struct TLV version_tlv;
	unsigned char *data=NULL;
	int dataLength;
	unsigned char version_data[1]={0x02};
	encode(version_data,1,0x02,&version_tlv);

	dataLength=version_tlv.length+version_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&version_tlv);

	encode(data,dataLength,0xA0,tlv);

}

void serialNumber(struct TLV *tlv)
{
	unsigned char certificateSerialNumber[9]={0x00, 0x9C, 0x5C, 0x68, 0x8F, 0xE5, 0x75, 0xC8, 0x57};	
	
	unsigned char temp[20],serialNumber[20]={0x00};
	int i,value=0;
	FILE *fp=NULL;
	fp=fopen("serialnumbers","r");
	if(fp){
		while(fgets(serialNumber,20,fp)!=NULL){
			memcpy(temp,serialNumber,strlen(serialNumber));
		}
		temp[strlen(temp)-1]='\0';
		value=atoi(temp);
		
		fclose(fp);

		

	}
	value+=1;
	
	sprintf(temp,"%d",value);
	fp=fopen("serialnumbers","a");
	if(fp){
		fwrite("\n",1,1,fp);
		fwrite(temp,strlen(temp),1,fp);
	fclose(fp);
	}
	encode(temp,strlen(temp),0x02,tlv);

}

void signatureAlgorithm(struct TLV *tlv)
{
	struct TLV algorithmIdentifier_tlv,null_tlv;
	unsigned char oid_algorithmIdentifier[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x0B};
	unsigned char *data=NULL;
	int dataLength,i;

	encode(oid_algorithmIdentifier,9,0x06,&algorithmIdentifier_tlv);
	encode(NULL,0,0x05,&null_tlv);

	dataLength=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	i=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1;

	tlvToStream(data,i,&algorithmIdentifier_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,tlv);
	free(data);
}

void signatureAlgorithmPSS(struct TLV *tlv)
{
	struct TLV algorithmIdentifier_tlv,null_tlv,oid_hash_algo_tlv,oid_mgf1_tlv;
	unsigned char oid_algorithmIdentifier_pss[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x0A};
	unsigned char oid_hash_algo[9]={0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01};
	unsigned char oid_mgf1[9]={0x2A, 0x86, 0x48, 0x86,  0xF7, 0x0D, 0x01, 0x01, 0x08};
	unsigned char default_oid[3]={0x02, 0x01, 0x20};
	struct TLV temp,temp1,temp2,temp3,temp_middle;
	
	unsigned char *data=NULL;
	int dataLength,i;
	encode(oid_algorithmIdentifier_pss,9,0x06,&algorithmIdentifier_tlv);
	
	encode(oid_hash_algo,9,0x06,&oid_hash_algo_tlv);
	encode(oid_mgf1,9,0x06,&oid_mgf1_tlv);
	dataLength=oid_hash_algo_tlv.length+oid_hash_algo_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&oid_hash_algo_tlv);
	encode(data,dataLength,0x30,&temp);
	
	if(data)
		free(data);
	
	dataLength=oid_mgf1_tlv.length+oid_mgf1_tlv.valueLength+1+temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_mgf1_tlv.length+oid_mgf1_tlv.valueLength+1;
	
	tlvToStream(data,i,&oid_mgf1_tlv);
	tlvToStream(data+i,dataLength-i,&temp);
	encode(data,dataLength,0x30,&temp1);
	
	if(data)
		free(data);

	dataLength=temp1.length+temp1.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&temp1);
	encode(data,dataLength,0xA1,&temp_middle);

	if(data)
		free(data);

	
	dataLength=oid_hash_algo_tlv.length+oid_hash_algo_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&oid_hash_algo_tlv);
	encode(data,dataLength,0x30,&temp);


	if(data)
		free(data);

	dataLength=temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&temp);
	encode(data,dataLength,0xA0,&temp2);

	encode(default_oid,3,0xA2,&temp3);

	
	if(data)
			free(data);

	dataLength=temp2.length+temp2.valueLength+1;
	dataLength+=temp_middle.length+temp_middle.valueLength+1;
	dataLength+=temp3.length+temp3.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	i=temp2.length+temp2.valueLength+1;

	tlvToStream(data,i,&temp2);
	tlvToStream(data+i,temp_middle.value+temp_middle.valueLength+1,&temp_middle);
	i=i+temp_middle.length+temp_middle.valueLength+1;
	tlvToStream(data+i,dataLength-i,&temp3);

	encode(data,dataLength,0x30,&temp);

	if(data)
		free(data);

	dataLength=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1+temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=algorithmIdentifier_tlv.length+algorithmIdentifier_tlv.valueLength+1;

	tlvToStream(data,i,&algorithmIdentifier_tlv);
	tlvToStream(data+i,dataLength-i,&temp);

	encode(data,dataLength,0x30,tlv);
	
	if(data)
		free(data);
}



void issuer()
{
	struct TLV oid_common_name_tlv,oid_country_name_tlv,oid_state_province_name_tlv,oid_organization_name_tlv,oid_organization_unit_name_tlv;
	struct TLV oid_common_name_data_tlv,oid_country_name_data_tlv,oid_state_province_name_data_tlv,oid_organization_name_data_tlv,oid_organization_unit_name_data_tlv;
	struct TLV oid_common_name_tlv_final,oid_country_name_tlv_final,oid_state_province_name_tlv_final,oid_organization_name_tlv_final,oid_organization_unit_name_tlv_final;
	struct TLV myIssuer;

	unsigned char *data=NULL,*data1=NULL,*data2=NULL,*dataFinal[5];

	int dataLength,data1Length,data2Length,i,dataLengthFinal[5];

	unsigned char oid_common_name[3]={0x55,0x04,0x03};
	unsigned char oid_country_name[3]={0x55,0x04,0x06};
	unsigned char oid_state_province_name[3]={0x55,0x04,0x08};
	unsigned char oid_organization_name[3]={0x55,0x04,0x0A};
	unsigned char oid_organization_unit_name[3]={0x55,0x04,0x0B};
	
	unsigned char oid_common_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_country_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_state_province_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_organization_name_data[3]={0x61,0x61,0x61};
	unsigned char oid_organization_unit_name_data[3]={0x61,0x61,0x61};

	encode(oid_common_name,3,0x06,&oid_common_name_tlv);
	encode(oid_country_name,3,0x06,&oid_country_name_tlv);
	encode(oid_state_province_name,3,0x06,&oid_state_province_name_tlv);
	encode(oid_organization_name,3,0x06,&oid_organization_name_tlv);
	encode(oid_organization_unit_name,3,0x06,&oid_organization_unit_name_tlv);

	encode(oid_common_name_data,3,0x13,&oid_common_name_data_tlv);
	encode(oid_country_name_data,3,0x13,&oid_country_name_data_tlv);
	encode(oid_state_province_name_data,3,0x13,&oid_state_province_name_data_tlv);
	encode(oid_organization_name_data,3,0x13,&oid_organization_name_data_tlv);
	encode(oid_organization_unit_name_data,3,0x13,&oid_organization_unit_name_data_tlv);

	data1Length=oid_common_name_tlv.length+oid_common_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_common_name_tlv);

	data2Length=oid_common_name_data_tlv.length+oid_common_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_common_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_common_name_tlv_final);
	free(data);
	free(data1);
	free(data2);


	data1Length=oid_country_name_tlv.length+oid_country_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_country_name_tlv);

	data2Length=oid_country_name_data_tlv.length+oid_country_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_country_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_country_name_tlv_final);
	free(data);
	free(data1);
	free(data2);

	data1Length=oid_state_province_name_tlv.length+oid_state_province_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_state_province_name_tlv);

	data2Length=oid_state_province_name_data_tlv.length+oid_state_province_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_state_province_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_state_province_name_tlv_final);
	free(data);
	free(data1);
	free(data2);



	data1Length=oid_organization_name_tlv.length+oid_organization_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_organization_name_tlv);

	data2Length=oid_organization_name_data_tlv.length+oid_organization_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_organization_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_organization_name_tlv_final);
	free(data);
	free(data1);
	free(data2);




	data1Length=oid_organization_unit_name_tlv.length+oid_organization_unit_name_tlv.valueLength+1;
	data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
	tlvToStream(data1,data1Length,&oid_organization_unit_name_tlv);

	data2Length=oid_organization_unit_name_data_tlv.length+oid_organization_unit_name_data_tlv.valueLength+1;
	data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
	tlvToStream(data2,data2Length,&oid_organization_unit_name_data_tlv);

	dataLength=data1Length+data2Length;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	for(i=0;i<data1Length;i++){
		data[i]=data1[i];
	}
	for(i=0;i<data2Length;i++){
		data[i+data1Length]=data2[i];
	}
	encode(data,dataLength,0x80,&oid_organization_unit_name_tlv_final);
	free(data);
	free(data1);
	free(data2);



	dataLengthFinal[0]=oid_common_name_tlv_final.length+oid_common_name_tlv_final.valueLength+1;
	dataFinal[0]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[0]);
	tlvToStream(dataFinal[0],dataLengthFinal[0],&oid_common_name_tlv_final);

	dataLengthFinal[1]=oid_country_name_tlv_final.length+oid_country_name_tlv_final.valueLength+1;
	dataFinal[1]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[1]);
	tlvToStream(dataFinal[1],dataLengthFinal[1],&oid_country_name_tlv_final);

	dataLengthFinal[2]=oid_state_province_name_tlv_final.length+oid_state_province_name_tlv_final.valueLength+1;
	dataFinal[2]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[2]);
	tlvToStream(dataFinal[2],dataLengthFinal[2],&oid_state_province_name_tlv_final);

	dataLengthFinal[3]=oid_organization_name_tlv_final.length+oid_organization_name_tlv_final.valueLength+1;
	dataFinal[3]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[3]);
	tlvToStream(dataFinal[3],dataLengthFinal[3],&oid_organization_name_tlv_final);

	dataLengthFinal[4]=oid_organization_unit_name_tlv_final.length+oid_organization_unit_name_tlv_final.valueLength+1;
	dataFinal[4]=(unsigned char*)malloc(sizeof(unsigned char)*dataLengthFinal[4]);
	tlvToStream(dataFinal[4],dataLengthFinal[4],&oid_organization_unit_name_tlv_final);

	dataLength=0;
	for(i=0;i<5;i++){
		dataLength+=dataLengthFinal[i];
	}

	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	int count=0;
	for(int j=0;j<5;j++){
		for(i=0;i<dataLengthFinal[j];i++){
			data[count++]=dataFinal[j][i];
		}
	}

	encode(data,dataLength,0x80,&myIssuer);
	free(data);

}

void issuerSubjectInfo(struct TLV *tlv, bool flag)
{
	int numberOfFields=9;
	unsigned char issuerValues[numberOfFields][30]={{"countryName"},{"stateOrProvinceName"},{"localityName"},{"organizationalUnitName"},{"organizationName"},{"commonName"},{"title"},{"description"}};
	unsigned char oids[numberOfFields][30]={{0x55,0x04,0x06},{0x55,0x04,0x08},{0x55,0x04,0x07},{0x55,0x04,0x0B},{0x55,0x04,0x0A},{0x55,0x04,0x03},{0x55,0x04,0x0C},{0x55,0x04,0x0D},{0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x09,0x01}};
	unsigned char oids_data[numberOfFields][100];
	
	struct TLV oids_tlv[numberOfFields],oids_data_tlv[numberOfFields],oids_final_tlv[numberOfFields],oids_final_set_tlv[numberOfFields];
	
	//unsigned char tempString[2]="aa";

	char *data=NULL,*data1=NULL,*data2=NULL;
	int dataLength,data1Length,data2Length;
	if(flag)
		printf("\n           Enter Issuer Details             ");
	else
		printf("\n           Enter Subject Details            ");	
	getchar();
	for(int i=0;i<numberOfFields;i++){
		printf("\n%s: ",issuerValues[i]);
		fgets(oids_data[i],30,stdin);
		oids_data[i][strcspn(oids_data[i],"\n")]=0;
	}

	for(int i=0;i<numberOfFields;i++){
		encode(oids[i],strlen(oids[i]),0x06,&oids_tlv[i]);
	}
	
	encode(oids_data[0],strlen(oids_data[0]),0x13,&oids_data_tlv[0]);
	for(int i=1;i<numberOfFields;i++){
		encode(oids_data[i],strlen(oids_data[i]),0x0C,&oids_data_tlv[i]);
	}

	

	for(int i=0;i<numberOfFields;i++){
		data1Length=oids_tlv[i].length+oids_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_tlv[i]);

		data2Length=oids_data_tlv[i].length+oids_data_tlv[i].valueLength+1;
		data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
		tlvToStream(data2,data2Length,&oids_data_tlv[i]);

		dataLength=data1Length+data2Length;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

		memcpy(data,data1,data1Length);
		memcpy(data+data1Length,data2,data2Length);

		encode(data,dataLength,0x30,&oids_final_tlv[i]);
		free(data1);
		free(data2);
		free(data);
	}

	for(int i=0;i<numberOfFields;i++){
		dataLength=oids_final_tlv[i].length+oids_final_tlv[i].valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		tlvToStream(data,dataLength,&oids_final_tlv[i]);
		encode(data,dataLength,0x31,&oids_final_set_tlv[i]);
		free(data);
	}
	dataLength=0;
	for(int i=0;i<numberOfFields;i++){
		dataLength+=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int j=0;
	for(int i=0;i<numberOfFields;i++){
		data1Length=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_final_set_tlv[i]);
		
		memcpy(data+j,data1,data1Length);
		j+=data1Length;
		free(data1);
	}

	encode(data,dataLength,0x30,tlv);
}

void issuerSubjectInfoFromConfigForSS(struct TLV *tlv, bool flag,char *fileName)
{
	int numberOfFields=9;
	//unsigned char issuerValues[numberOfFields][30]={{"IcountryName"},{"IstateOrProvinceName"},{"IlocalityName"},{"IorganizationalUnitName"},{"IorganizationName"},{"IcommonName"},{"Ititle"},{"Idescription"},{"IemailAddress"}};
	//unsigned char subjectValues[numberOfFields][30]={{"ScountryName"},{"SstateOrProvinceName"},{"SlocalityName"},{"SorganizationalUnitName"},{"SorganizationName"},{"ScommonName"},{"Stitle"},{"Sdescription"},{"SemailAddress"}};
	unsigned char issuerSubjectValues[numberOfFields][30]={{"countryName"},{"stateOrProvinceName"},{"localityName"},{"organizationalUnitName"},{"organizationName"},{"commonName"},{"title"},{"description"},{"emailAddress"}};
	unsigned char oids[numberOfFields][30]={{0x55,0x04,0x06},{0x55,0x04,0x08},{0x55,0x04,0x07},{0x55,0x04,0x0B},{0x55,0x04,0x0A},{0x55,0x04,0x03},{0x55,0x04,0x0C},{0x55,0x04,0x0D},{0x2A,0x86,0x48,0x86,0xF7,0x0D,0x01,0x09,0x01}};
	unsigned char oids_data[numberOfFields][100]={0x00};
	bool include_oids[9]={true,false,true,true,true,true,false,false,false};
	struct TLV oids_tlv[numberOfFields],oids_data_tlv[numberOfFields],oids_final_tlv[numberOfFields],oids_final_set_tlv[numberOfFields];

	FILE *fp=NULL;
	char line[100],*key,*value;
	
	
	char *data=NULL,*data1=NULL,*data2=NULL;
	int dataLength,data1Length,data2Length;
	/*if(flag)
		printf("\n           Enter Issuer Details             ");
	else
		printf("\n           Enter Subject Details            ");	
	*/

	fp=fopen(fileName,"r");
	if(fp==NULL){
		printf("\nOpening of file %s is failed",fileName);
	}
	if(fp!=NULL){

		while(fgets(line,sizeof(line),fp)!=NULL){
			key=strtok(line,"=");
			if((key!=NULL)&&(strlen(key)!=0)){
				key[strlen(key)-1]='\0';
			}
			
			value=strtok(NULL,"\n");
			
			
			//if(flag) {
			for(int i=0;i<numberOfFields;i++){
				if(strcmp(issuerSubjectValues[i],key)==0){
					strcpy(oids_data[i],value+1);
				}
			}
		/*}
		else {
			for(int i=0;i<numberOfFields;i++){
				if(strcmp(subjectValues[i],key)==0){
					strcpy(oids_data[i],value);
				}
			}	
		}*/

	}
 	}
	
	if(fp)
	{
		fclose(fp);
	}
	
	/*printf("\nDetails are:\n");

	for(int i=0;i<numberOfFields;i++){
		printf("\n%s:%s\n",issuerSubjectValues[i],oids_data[i]);
	}*/
	
	for(int i=0;i<numberOfFields;i++){
		encode(oids[i],strlen(oids[i]),0x06,&oids_tlv[i]);
	}
	
	encode(oids_data[0],strlen(oids_data[0]),0x13,&oids_data_tlv[0]);
	for(int i=1;i<numberOfFields;i++){
		encode(oids_data[i],strlen(oids_data[i]),0x0C,&oids_data_tlv[i]);
	}

	for(int i=0;i<numberOfFields;i++){

		data1Length=oids_tlv[i].length+oids_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_tlv[i]);

		data2Length=oids_data_tlv[i].length+oids_data_tlv[i].valueLength+1;
		data2=(unsigned char*)malloc(sizeof(unsigned char)*data2Length);
		tlvToStream(data2,data2Length,&oids_data_tlv[i]);

		dataLength=data1Length+data2Length;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

		memcpy(data,data1,data1Length);
		memcpy(data+data1Length,data2,data2Length);

		encode(data,dataLength,0x30,&oids_final_tlv[i]);
		free(data1);
		free(data2);
		free(data);
	}

	for(int i=0;i<numberOfFields;i++){
		dataLength=oids_final_tlv[i].length+oids_final_tlv[i].valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		tlvToStream(data,dataLength,&oids_final_tlv[i]);
		encode(data,dataLength,0x31,&oids_final_set_tlv[i]);
		free(data);
	}
	dataLength=0;
	for(int i=0;i<numberOfFields;i++){
		if(include_oids[i])
		dataLength+=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int j=0;
	for(int i=0;i<numberOfFields;i++){
		if(include_oids[i]){
		data1Length=oids_final_set_tlv[i].length+oids_final_set_tlv[i].valueLength+1;
		data1=(unsigned char*)malloc(sizeof(unsigned char)*data1Length);
		tlvToStream(data1,data1Length,&oids_final_set_tlv[i]);
		
		memcpy(data+j,data1,data1Length);
		j+=data1Length;
		free(data1);
	}
	}

	encode(data,dataLength,0x30,tlv);
}

void validity(struct TLV *tlv)
{
	char buff[20];
	//unsigned char utcTimeNotBefore[13],utcTimeNotAfter[13];
	unsigned char generalizedTimeBefore[15],generalizedTimeAfter[15];
	
	struct TLV notBefore_tlv,notAfter_tlv;

	unsigned char *data;
	int dataLength;
	
	time_t now = time(NULL);
	strftime(buff, 20, "%Y%m%d%H%M%S", localtime(&now));
	//buff[5]=buff[5]-1; //adjusting the time to 1 day prior to avoid clock synchronization while veryfying
	//printf("\n%s",buff);
 /*
	
	for(int i=0;i<12;i++){
		utcTimeNotBefore[i]=buff[i]|0x30;
		utcTimeNotAfter[i]=buff[i]|0x30;
	}
	utcTimeNotBefore[12]='Z';
	utcTimeNotAfter[12]='Z';

	utcTimeNotAfter[1]+=0x01;
 */
	for(int i=0;i<14;i++){
		generalizedTimeBefore[i]=buff[i]|0x30;
		generalizedTimeAfter[i]=buff[i]|0x30;
	}
		generalizedTimeBefore[14]='Z';
		generalizedTimeAfter[14]='Z';

		generalizedTimeAfter[0]=0x02|0x30;
		generalizedTimeAfter[1]=0x00|0x30;
		generalizedTimeAfter[2]=0x06|0x30;
		generalizedTimeAfter[3]=0x08|0x30;

	//encode(utcTimeNotBefore,13,0x17,&notBefore_tlv);
	//encode(utcTimeNotAfter,13,0x17,&notAfter_tlv);
	
	encode(generalizedTimeBefore,15,0x18,&notBefore_tlv);
	encode(generalizedTimeAfter,15,0x18,&notAfter_tlv);


	dataLength=notBefore_tlv.length+notBefore_tlv.valueLength+1+notAfter_tlv.length+notAfter_tlv.valueLength+1;
	
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	int temp=notBefore_tlv.length+notBefore_tlv.valueLength+1;
	tlvToStream(data,temp,&notBefore_tlv);
	tlvToStream(data+temp,dataLength-temp,&notAfter_tlv);
	

	encode(data,dataLength,0x30,tlv);
		
}

struct TLV subjectPublicKeyInfo(struct TLV *subjectPublicKeyInfo_tlv,unsigned char modulus[],int modulus_length,unsigned char exponent[],int exponent_length)
{
	unsigned char oid_rsa_algo[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01};
	struct TLV oid_rsa_algo_tlv,null_tlv,oid_rsa_algo_final_tlv,oid_rsa_key_data_tlv,oid_rsa_key_data_final_tlv,oid_rsa_modulus_data_tlv,oid_rsa_exponent_data_tlv,oid_rsa_key_tlv;
	int oid_rsa_algo_length=9;
	unsigned char *data=NULL;
	int dataLength,i=0;

	encode(oid_rsa_algo,oid_rsa_algo_length,0x06,&oid_rsa_algo_tlv);
	encode(NULL,0,0x05,&null_tlv);
	
	dataLength=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,&oid_rsa_algo_final_tlv);
	free(data);

	

	encode(modulus,modulus_length,0x02,&oid_rsa_modulus_data_tlv);
	encode(exponent,exponent_length,0x02,&oid_rsa_exponent_data_tlv);

	dataLength=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1+oid_rsa_exponent_data_tlv.length+oid_rsa_exponent_data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_modulus_data_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_exponent_data_tlv);

	encode(data,dataLength,0x30,&oid_rsa_key_data_tlv);

	

	free(data);

	dataLength=oid_rsa_key_data_tlv.length+oid_rsa_key_data_tlv.valueLength+1+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	data[0]=0x00;
	tlvToStream(data+1,dataLength,&oid_rsa_key_data_tlv);
	encode(data,dataLength,0x03,&oid_rsa_key_data_final_tlv);

	
	free(data);

	dataLength=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1+oid_rsa_key_data_final_tlv.length+oid_rsa_key_data_final_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_final_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_key_data_final_tlv);

	encode(data,dataLength,0x30,subjectPublicKeyInfo_tlv);

	return oid_rsa_key_data_tlv;

}

void subjectPublicKeyInfoFromFile(struct TLV *subjectPublicKeyInfo_tlv,char *fileName,unsigned char exponent[],int exponent_length)
{
	unsigned char oid_rsa_algo[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01};
	struct TLV oid_rsa_algo_tlv,null_tlv,oid_rsa_algo_final_tlv,oid_rsa_key_data_tlv,oid_rsa_key_data_final_tlv,oid_rsa_modulus_data_tlv,oid_rsa_exponent_data_tlv,oid_rsa_key_tlv;
	int oid_rsa_algo_length=9;
	unsigned char *data=NULL;
	int dataLength,i=0;

	encode(oid_rsa_algo,oid_rsa_algo_length,0x06,&oid_rsa_algo_tlv);
	encode(NULL,0,0x05,&null_tlv);
	
	dataLength=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_tlv.length+oid_rsa_algo_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_tlv);
	tlvToStream(data+i,dataLength-i,&null_tlv);

	encode(data,dataLength,0x30,&oid_rsa_algo_final_tlv);
	free(data);

	
	readModulus(&oid_rsa_modulus_data_tlv,fileName);
	//encode(modulus,modulus_length,0x02,&oid_rsa_modulus_data_tlv);
	encode(exponent,exponent_length,0x02,&oid_rsa_exponent_data_tlv);

	dataLength=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1+oid_rsa_exponent_data_tlv.length+oid_rsa_exponent_data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_modulus_data_tlv.length+oid_rsa_modulus_data_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_modulus_data_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_exponent_data_tlv);

	encode(data,dataLength,0x30,&oid_rsa_key_data_tlv);

	free(data);

	dataLength=oid_rsa_key_data_tlv.length+oid_rsa_key_data_tlv.valueLength+1+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	data[0]=0x00;
	tlvToStream(data+1,dataLength,&oid_rsa_key_data_tlv);
	encode(data,dataLength,0x03,&oid_rsa_key_data_final_tlv);

	free(data);

	dataLength=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1+oid_rsa_key_data_final_tlv.length+oid_rsa_key_data_final_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=oid_rsa_algo_final_tlv.length+oid_rsa_algo_final_tlv.valueLength+1;

	tlvToStream(data,i,&oid_rsa_algo_final_tlv);
	tlvToStream(data+i,dataLength-i,&oid_rsa_key_data_final_tlv);

	encode(data,dataLength,0x30,subjectPublicKeyInfo_tlv);

}


void extensions(struct TLV *tlv,bool flag)
{
	struct TLV keyUsage_tlv,basicConstraints_tlv,subjectKeyIdentifier_tlv,authorityKeyIdentifier_tlv,subjectKeyIdentifier_oid,authorityKeyIdentifier_oid,extensions_tlv;
	unsigned char *data=NULL;
	int dataLength;
	unsigned char subjectKeyIdentifier[3]={0x55, 0x1D, 0x0E},authorityKeyIdentifier[3]={0x55, 0x1D, 0x23};

	encode(subjectKeyIdentifier,sizeof(subjectKeyIdentifier),0x06,&subjectKeyIdentifier_oid);
	encode(authorityKeyIdentifier,sizeof(authorityKeyIdentifier),0x06,&authorityKeyIdentifier_oid);
	
	

	keyUsage(&keyUsage_tlv,flag);

	if(flag)
	{
		basicConstraints(&basicConstraints_tlv,flag);

	dataLength=keyUsage_tlv.length+keyUsage_tlv.valueLength+1+basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,keyUsage_tlv.length+keyUsage_tlv.valueLength+1,&keyUsage_tlv);
	tlvToStream(data+keyUsage_tlv.length+keyUsage_tlv.valueLength+1,basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1,&basicConstraints_tlv);

	}
	else
	{


	dataLength=keyUsage_tlv.length+keyUsage_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,keyUsage_tlv.length+keyUsage_tlv.valueLength+1,&keyUsage_tlv);


	}	

	
	encode(data,dataLength,0x30,&extensions_tlv);

	tlvToStream1(&data,&dataLength,&extensions_tlv);

	encode(data,dataLength,0xA3,tlv);

}

void my_extensions(struct TLV *tlv,bool flag,unsigned char* dgst, unsigned int dgstLen,unsigned char* dgst_a, unsigned int dgstLen_a)
{
	struct TLV keyUsage_tlv,basicConstraints_tlv,subjectKeyIdentifier_tlv,authorityKeyIdentifier_tlv;
	struct TLV subjectKeyIdentifier_oid,authorityKeyIdentifier_oid,subjectKeyIdentifier_data,authorityKeyIdentifier_data,extensions_tlv;
	unsigned char *data=NULL;
	int dataLength,temp;
	unsigned char subjectKeyIdentifier[3]={0x55, 0x1D, 0x0E},authorityKeyIdentifier[3]={0x55, 0x1D, 0x23};

	encode(subjectKeyIdentifier,sizeof(subjectKeyIdentifier),0x06,&subjectKeyIdentifier_oid);
	encode(authorityKeyIdentifier,sizeof(authorityKeyIdentifier),0x06,&authorityKeyIdentifier_oid);

	
	encode(dgst,dgstLen,0x04,&subjectKeyIdentifier_data);
	dataLength=subjectKeyIdentifier_data.length+subjectKeyIdentifier_data.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&subjectKeyIdentifier_data);
	encode(data,dataLength,0x04,&subjectKeyIdentifier_data);
	free(data);

	dataLength=subjectKeyIdentifier_oid.length+subjectKeyIdentifier_oid.valueLength+1+subjectKeyIdentifier_data.length+subjectKeyIdentifier_data.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,subjectKeyIdentifier_oid.length+subjectKeyIdentifier_oid.valueLength+1,&subjectKeyIdentifier_oid);
	tlvToStream(data+subjectKeyIdentifier_oid.length+subjectKeyIdentifier_oid.valueLength+1,subjectKeyIdentifier_data.length+subjectKeyIdentifier_data.valueLength+1,&subjectKeyIdentifier_data);
	encode(data,dataLength,0x30,&subjectKeyIdentifier_tlv);
	free(data);

	// authroirty key identifier
	
	encode(dgst_a,dgstLen_a,0x80,&authorityKeyIdentifier_data);
	dataLength=authorityKeyIdentifier_data.length+authorityKeyIdentifier_data.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&authorityKeyIdentifier_data);
	encode(data,dataLength,0x30,&authorityKeyIdentifier_data);
	free(data);
	dataLength=authorityKeyIdentifier_data.length+authorityKeyIdentifier_data.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&authorityKeyIdentifier_data);
	encode(data,dataLength,0x04,&authorityKeyIdentifier_data);
	free(data);

	dataLength=authorityKeyIdentifier_oid.length+authorityKeyIdentifier_oid.valueLength+1+authorityKeyIdentifier_data.length+authorityKeyIdentifier_data.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,authorityKeyIdentifier_oid.length+authorityKeyIdentifier_oid.valueLength+1,&authorityKeyIdentifier_oid);
	tlvToStream(data+authorityKeyIdentifier_oid.length+authorityKeyIdentifier_oid.valueLength+1,authorityKeyIdentifier_data.length+authorityKeyIdentifier_data.valueLength+1,&authorityKeyIdentifier_data);
	encode(data,dataLength,0x30,&authorityKeyIdentifier_tlv);
	free(data);

	keyUsage(&keyUsage_tlv,flag);
	basicConstraints(&basicConstraints_tlv,flag);
	
	if(flag)
	{	
		dataLength=subjectKeyIdentifier_tlv.length+subjectKeyIdentifier_tlv.valueLength+1+authorityKeyIdentifier_tlv.length+authorityKeyIdentifier_tlv.valueLength+1+keyUsage_tlv.length+keyUsage_tlv.valueLength+1+basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1;
	}
	else
	{
		dataLength=subjectKeyIdentifier_tlv.length+subjectKeyIdentifier_tlv.valueLength+1+authorityKeyIdentifier_tlv.length+authorityKeyIdentifier_tlv.valueLength+1+basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1;
	}


	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1,&basicConstraints_tlv);
	temp=basicConstraints_tlv.length+basicConstraints_tlv.valueLength+1;

	if(flag)
	{	
	tlvToStream(data+temp,keyUsage_tlv.length+keyUsage_tlv.valueLength+1,&keyUsage_tlv);
	temp+=keyUsage_tlv.length+keyUsage_tlv.valueLength+1;
	}

	tlvToStream(data+temp,subjectKeyIdentifier_tlv.length+subjectKeyIdentifier_tlv.valueLength+1,&subjectKeyIdentifier_tlv);
	temp+=subjectKeyIdentifier_tlv.length+subjectKeyIdentifier_tlv.valueLength+1;
	
	tlvToStream(data+temp,authorityKeyIdentifier_tlv.length+authorityKeyIdentifier_tlv.valueLength+1,&authorityKeyIdentifier_tlv);
	
	encode(data,dataLength,0x30,&extensions_tlv);
	tlvToStream1(&data,&dataLength,&extensions_tlv);

	encode(data,dataLength,0xA3,tlv);

	printf("\nExtension TLV:\n");
	display(tlv);

}

void tbsder(struct TLV *tlv,bool flag)
{
	struct TLV temp[10];
	
	unsigned char *data=NULL;
	int dataLength,i;
	int numberOfFields=8;

	version(&temp[0]);      //version(&version_tlv);
	serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
	signatureAlgorithm(&temp[2]); //signature(&signature_tlv);
	issuerSubjectInfo(&temp[3],true); //issuerSubjectInfo(&issuer_tlv);
	validity(&temp[4]); //validaity(&validity_tlv);
	issuerSubjectInfo(&temp[5],false); //issuerSubjectInfo(&subject_tlv);
	subjectPublicKeyInfo(&temp[6],NULL,0,NULL,0); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
	extensions(&temp[7],flag); //extensions(&extensions_tlv);

	dataLength=0;
	for(i=0;i<numberOfFields;i++){
		dataLength+=temp[i].length+temp[i].valueLength+1;
		//printf("\ndataLength=%d",dataLength);
	}
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	if(data==NULL){
		printf("\nMemory allocation failed");
	}
	int t=0;
	for(i=0;i<numberOfFields;i++){
		//printf("\nt=%d dataLength=%d",t,dataLength);

		tlvToStream(data+t,temp[i].length+temp[i].valueLength+1,&temp[i]);
		t+=temp[i].length+temp[i].valueLength+1;
	}
	encode(data,dataLength,0x30,tlv);
	free(data);

}

void keyUsage(struct TLV *tlv,bool flag){
	unsigned char keyBits[3]={0x07,0x00,0x00};
	unsigned char keyUsage_oid[3]={0x55, 0x1D, 0x0F};
	struct TLV keyUsage_oid_tlv,bool_tlv,keyBits_tlv;
	unsigned char *data=NULL;
	int dataLength;	
	unsigned char true_value[1]={0xFF},false_value[1]={0x00};
	if(flag)
		keyBits[1]^=0xC4; //This is the hardcoded value for CA for digital signature, non repudiation, keycert sign
	else
		keyBits[1]^=0xC0;

	encode(keyBits,3,0x03,&keyBits_tlv);
	dataLength=keyBits_tlv.length+keyBits_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&keyBits_tlv);

	encode(data,dataLength,0x04,&keyBits_tlv);
	free(data);

	encode(keyUsage_oid,3,0x06,&keyUsage_oid_tlv);
	if(flag)
		encode(true_value,1,0x01,&bool_tlv);
	else
		encode(false_value,1,0x01,&bool_tlv);

	dataLength=keyBits_tlv.length+keyBits_tlv.valueLength+1+bool_tlv.length+bool_tlv.valueLength+1+keyUsage_oid_tlv.length+keyUsage_oid_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	

	int temp=keyUsage_oid_tlv.length+keyUsage_oid_tlv.valueLength+1;
	tlvToStream(data,temp,&keyUsage_oid_tlv);
	tlvToStream(data+temp,bool_tlv.length+bool_tlv.valueLength+1,&bool_tlv);
	temp+=bool_tlv.length+bool_tlv.valueLength+1;
	tlvToStream(data+temp,keyBits_tlv.length+keyBits_tlv.valueLength+1,&keyBits_tlv);

	encode(data,dataLength,0x30,tlv);
}

void basicConstraints(struct TLV *tlv,bool flag){
	struct TLV basicConstraints_oid_tlv,bool_tlv,temp;
	unsigned char basicConstraints_oid_data[3]={0x55, 0x1D, 0x13};
	unsigned char *data=NULL;
	unsigned char true_value[1]={0xFF},false_value[1]={0x00};
	int dataLength;

	encode(basicConstraints_oid_data,3,0x06,&basicConstraints_oid_tlv);
	if(flag)
		encode(true_value,1,0x01,&bool_tlv);
	else
		encode(false_value,1,0x01,&bool_tlv);
	if(flag)
		encode(true_value,1,0x01,&temp);
	else
		encode(false_value,1,0x01,&temp);

	if(flag)
	{	
		tlvToStream1(&data,&dataLength,&temp);
		encode(data,dataLength,0x30,&temp);
		tlvToStream1(&data,&dataLength,&temp);
		encode(data,dataLength,0x04,&temp);
		dataLength=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1+bool_tlv.length+bool_tlv.valueLength+1+temp.length+temp.valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		int i=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1;
		tlvToStream(data,i,&basicConstraints_oid_tlv);
		tlvToStream(data+i,bool_tlv.length+bool_tlv.valueLength+1,&bool_tlv);
		i+=bool_tlv.length+bool_tlv.valueLength+1;
		tlvToStream(data+i,temp.length+temp.valueLength+1,&temp);
		encode(data,dataLength,0x30,tlv);
		
	}
	else
	{	
		encode(NULL,0,0x30,&temp);
		tlvToStream1(&data,&dataLength,&temp);
		encode(data,dataLength,0x04,&temp);
		
		dataLength=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1+temp.length+temp.valueLength+1;
		data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
		int i=basicConstraints_oid_tlv.length+basicConstraints_oid_tlv.valueLength+1;
		tlvToStream(data,i,&basicConstraints_oid_tlv);
		tlvToStream(data+i,temp.length+temp.valueLength+1,&temp);

		encode(data,dataLength,0x30,tlv);
		

	}
		
	printf("\nBasic contraints TLV:\n");
	display(tlv);


}

int getVal(char ch)
{
	if ((ch >= '0') && (ch <= '9')){
		return ch - '0';
	}
	else if((ch >= 'a') && (ch <= 'f')){
		return ch - 'a' +10;
	}
	else if((ch >= 'A') && (ch <= 'F')){
		return ch - 'A' +10;
	}	
	 	
	
}
void readModulus(struct TLV *tlv, char *fileName)
{
	FILE *fp=NULL;
	fp=fopen(fileName,"r");
	unsigned char key[257];
	int i=1;
	char ch,ch1;
	key[0]=0x00;
	if(fp){
		while((ch=fgetc(fp))!=EOF){
			ch1=fgetc(fp);
			if(ch1==EOF)
				break;
			key[i++]=getVal(ch)*16+getVal(ch1);
			}
		fclose(fp);
		encode(key,257,0x02,tlv);

	}
}


// PKCS7 RFC

void pkcs7_signedData(struct TLV *tlv)
{

	unsigned char oid_algorithmIdentifier[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x07, 0x02};
	encode(oid_algorithmIdentifier,9,0x06,tlv);


}

void pkcs7_data(struct TLV *tlv)
{
	unsigned char oid_algorithmIdentifier[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x07, 0x01};
	encode(oid_algorithmIdentifier,9,0x06,tlv);
}

void signingTime(struct TLV *tlv)
{
	char buff[20];

	unsigned char oid_signTimeIdentifier[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x09, 0x05};

	unsigned char generalizedTimeBefore[15],*data;
	int dataLength;
	struct TLV notBefore_tlv,temp,signTime_tlv;
	time_t now = time(NULL);
	strftime(buff, 20, "%y%m%d%H%M%S", localtime(&now));
	buff[5]=buff[5]-1; //adjusting the time to 1 day prior to avoid clock synchronization while veryfying

	
	
	for(int i=0;i<12;i++){
		generalizedTimeBefore[i]=buff[i]|0x30;
	
	}
	generalizedTimeBefore[i]='Z';
	encode(generalizedTimeBefore,13,0x17,&notBefore_tlv);



	
	dataLength=notBefore_tlv.length+notBefore_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	tlvToStream(data,dataLength,&notBefore_tlv);

	encode(data,dataLength,0x31,&temp);

	free(data);
		
	
	encode(oid_signTimeIdentifier,9,0x06,&signTime_tlv);
	dataLength=signTime_tlv.length+signTime_tlv.valueLength+1+temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,signTime_tlv.length+signTime_tlv.valueLength+1,&signTime_tlv);
	tlvToStream(data+signTime_tlv.length+signTime_tlv.valueLength+1,temp.length+temp.valueLength+1,&temp);

	encode(data,dataLength,0x30,tlv);
	
	
}

void contentType(struct TLV *tlv)
{
	unsigned char oid_contentType[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01,  0x09, 0x03},oid_data[9]={0x2A, 0x86,
0x48, 0x86, 0xF7, 0x0D, 0x01, 0x07, 0x01 };

	struct TLV data_tlv,contentType_tlv,temp;

	unsigned char *data=NULL;
	int dataLength;

	encode(oid_contentType,9,0x06,&contentType_tlv);
	encode(oid_data,9,0x06,&data_tlv);

	dataLength=data_tlv.length+data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&data_tlv);

	encode(data,dataLength,0x31,&data_tlv);
	free(data);

	dataLength=contentType_tlv.length+contentType_tlv.valueLength+1+data_tlv.length+data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,contentType_tlv.length+contentType_tlv.valueLength+1,&contentType_tlv);
	tlvToStream(data+contentType_tlv.length+contentType_tlv.valueLength+1,data_tlv.length+data_tlv.valueLength+1,&data_tlv);

	encode(data,dataLength,0x30,tlv);

	free(data);
	
}

void messageDigest(struct TLV *tlv,unsigned char *hash)
{
	unsigned char oid_messageDigest[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x09, 0x04};
	struct TLV messageDigest_tlv,hash_tlv;

	unsigned char *data=NULL;
	int dataLength;

	
	encode(oid_messageDigest,9,0x06,&messageDigest_tlv);

	encode(hash,32,0x04,&hash_tlv);

	dataLength=hash_tlv.length+hash_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&hash_tlv);

	encode(data,dataLength,0x31,&hash_tlv);

	free(data);

	dataLength=messageDigest_tlv.length+messageDigest_tlv.valueLength+1+hash_tlv.length+hash_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,messageDigest_tlv.length+messageDigest_tlv.valueLength+1,&messageDigest_tlv);
	tlvToStream(data+messageDigest_tlv.length+messageDigest_tlv.valueLength+1,hash_tlv.length+hash_tlv.valueLength+1,&hash_tlv);
	
	encode(data,dataLength,0x30,tlv);

	free(data);
	
}

void pkcs7_digestAlgorithm(struct TLV *tlv)
{
	unsigned char oid_digestAlgorithm[9]={0x60, 0x86, 0x48, 0x01, 0x65, 0x03,  0x04, 0x02, 0x01};
	struct TLV digestAlgorithm_tlv;
	unsigned char *data=NULL;
	int dataLength;
	encode(oid_digestAlgorithm,9,0x06,&digestAlgorithm_tlv);
	dataLength=digestAlgorithm_tlv.length+digestAlgorithm_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&digestAlgorithm_tlv);
	encode(data,dataLength,0x30,tlv);
	free(data);
}

void pkcs7_encryptionAlgorithm(struct TLV *tlv)
{
	unsigned char oid_encryptionAlgorithm[9]={0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01};
	struct TLV encryptionAlgorithm_tlv,null_tlv;

	unsigned char *data=NULL;
	int dataLength;

	encode(oid_encryptionAlgorithm,9,0x06,&encryptionAlgorithm_tlv);
	encode(NULL,0,0x05,&null_tlv);

	dataLength=encryptionAlgorithm_tlv.length+encryptionAlgorithm_tlv.valueLength+1+null_tlv.length+null_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,encryptionAlgorithm_tlv.length+encryptionAlgorithm_tlv.valueLength+1,&encryptionAlgorithm_tlv);
	tlvToStream(data+encryptionAlgorithm_tlv.length+encryptionAlgorithm_tlv.valueLength+1,null_tlv.length+null_tlv.valueLength+1,&null_tlv);

	encode(data,dataLength,0x30,tlv);

	free(data);
	
}

void pkcs7_randomNumber(struct TLV *tlv)
{
	unsigned char randNum[9];
	memset(randNum,0x00,9);
	encode(randNum,9,0x02,tlv);
}

void pkcs7_rsaSignature(struct TLV *tlv,unsigned char *sig)
{
	
	encode(sig,256,0x04,tlv);
}

void pkcs7_version(struct TLV *tlv)
{
	
	unsigned char version_data[1]={0x01};
	encode(version_data,1,0x02,tlv);
	
}


void readHashSignature(unsigned char *hash,unsigned char *sig,char *fileName)
{
	//A3DD6B5A79A0FC8A689CAD17C1D5EA32B70AA125710685A1C1BFEC2D5478E7E8
	//27E67DC33DD3B1F485FBCEA523D7BFFF12D0EA6263C5178D5A6F23B11CDF545401E6CF55420894549F04D9576C1C74F1707E111B0E5FBF3EA9C17EB80793A2C36C96BEAA790326BFF1D77675DE35FC4933F492676FDF0A45EB1F86EF8B7E874FE3F2208107CD09E31D8E99CF057B220A8BB5DC138162995D9502A5EE78A085C8A535DE0C4BFD96B73357FC2F291EADE34CD1F21EF3E4A0494F275200F4ADE74903582DD9297023543BB73D7E59B5D926588E361364DF62DD04C2512B954EA73F095770BAE4E2F1F00609A2D5E9BED94E302063F20948919054FFE39BFFDB9FBD650C77F4BEC9EE875119CCAB1AC290DFB9F2DB2259134AE2C6E10925882ECD3A
	int numberOfFields=2;
	unsigned char hashsigvalues[numberOfFields][30]={{"hash"},{"signature"}};
	unsigned char data[numberOfFields][512]={0x00};
	unsigned char hex_data[numberOfFields][512]={0x00};
	int t1,t2,k;
	
	FILE *fp=NULL;
	char line[600],*key,*value;
	
	
	fp=fopen(fileName,"r");
	if(fp==NULL){
		printf("\nOpening of file %s is failed",fileName);
	}
	if(fp!=NULL){

		while(fgets(line,sizeof(line),fp)!=NULL){
			key=strtok(line,"=");
			if((key!=NULL)&&(strlen(key)!=0)){
				key[strlen(key)-1]='\0';
			}
			
			value=strtok(NULL,"\n");
			
			
			for(int i=0;i<numberOfFields;i++){
				if(strcmp(hashsigvalues[i],key)==0){
					strcpy(data[i],value+1);
				}
			}

 		}
	}
	
	if(fp)
	{
		fclose(fp);
	}
	
	for(int i=0;i<numberOfFields;i++)
	{
		k=0;
		for(int j=0;j<strlen(data[i]);j+=2)
		{
			if(isdigit(data[i][j]))
				t1=data[i][j]-'0';
			else
				t1=data[i][j]-'A'+10;

			if(isdigit(data[i][j+1]))
				t2=data[i][j+1]-'0';
			else
				t2=data[i][j+1]-'A'+10;

			hex_data[i][k++]=(unsigned char)t1*16+t2;
		}

	}

		memcpy(hash, hex_data[0],32);
		memcpy(sig, hex_data[1],256);

}


