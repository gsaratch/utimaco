#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#define SEQUENCE 0x30
#define INTEGER 0x02

struct TLV{
	unsigned char tag;
	unsigned char *lengthBytes;
	unsigned int length;
	unsigned char *value;
	unsigned int valueLength;
};
void encode(unsigned char *data,int inLength, unsigned char tagValue,struct TLV *tlv);
void display(struct TLV *tlv);
void tlvToStream(unsigned char *data, int length, struct TLV *tlv);

void version(struct TLV *tlv);
void serialNumber(struct TLV *tlv);
void signatureAlgorithm(struct TLV *tlv);
void signatureAlgorithmPSS(struct TLV *tlv);

void issuer();
void issuerSubjectInfo(struct TLV *tlv,bool flag);
void validity(struct TLV *tlv);

//void subjectPublicKeyInfo(struct TLV *subjectPublicKeyInfo_tlv,unsigned char modulus[],int modulus_length,unsigned char exponent[],int exponent_length);
struct TLV subjectPublicKeyInfo(struct TLV *subjectPublicKeyInfo_tlv,unsigned char modulus[],int modulus_length,unsigned char exponent[],int exponent_length);

void extensions(struct TLV *extensions_tlv,bool caFlag);

void my_extensions(struct TLV *tlv,bool flag,unsigned char* dgst, unsigned int dgstLen,unsigned char* dgst_a, unsigned int dgstLen_a);


void tbsder(struct TLV *tlv,bool caFlag);

void issuerSubjectInfoFromConfigForSS(struct TLV *tlv, bool flag,char *fileName);
void keyUsage(struct TLV *tlv,bool caFlag);
void tlvToStream1(unsigned char **data, int *length, struct TLV *tlv);
void basicConstraints(struct TLV *tlv,bool caFlag);
void readModulus(struct TLV *tlv, char *fileName);
void subjectPublicKeyInfoFromFile(struct TLV *subjectPublicKeyInfo_tlv,char *fileName,unsigned char exponent[],int exponent_length);

void pkcs7_signedData(struct TLV *tlv);
void pkcs7_data(struct TLV *tlv);
void signingTime(struct TLV *tlv);
void contentType(struct TLV *tlv);
void messageDigest(struct TLV *tlv,unsigned char *hash);
void pkcs7_digestAlgorithm(struct TLV *tlv);
void pkcs7_encryptionAlgorithm(struct TLV *tlv);
void pkcs7_randomNumber(struct TLV *tlv);
void pkcs7_rsaSignature(struct TLV *tlv,unsigned char *sig);
void pkcs7_version(struct TLV *tlv);
void readHashSignature(unsigned char *hash,unsigned char *sig,char *fileName);


