#! /bin/sh
# JSHIMA: Script has been modified to only generate two new certificate levels using original DEVELOPMENT ca_cert (root ca)
# v3
#

print_usage () {
  echo "Usage: " $0 " all | clean"
  echo "Example: sh" $0 "all"
}

createCApair_func() {
  echo "Creating certificate authority aka CA key and certificate"
  init_func
  ###############################################################################################
  # Create certificate authority (aka CA), private key and certificate
  ###############################################################################################
  #openssl req -nodes -new -x509 -newkey rsa:2048 -sha256 -days 1825 -extensions v3_ca -subj /C=IL/ST=NI/L="Location"/OU="R&D"/O=TELIT/CN="ca_cert" -keyout ca_private.key -out ca_cert.crt
  #if [ $? -eq 0 ]; then
    echo "CA key and cert created."
  #else
  #  echo "CA key and cert creation failed."
  #  exit
  #fi    
}

create_attest_ca_cert_CSR_func() {
  echo "Running create_attest_ca_cert_CSR_func"
  ###############################################################################################
  # Create server private key and certificate signing request.
  ###############################################################################################
  openssl req -new -nodes -newkey rsa:2048 -sha256 -days 730 -subj /C=IL/ST=NI/L="Location"/OU="R&D"/O=TELIT/CN="attest_ca" -keyout attest_ca_private.key -out attest_ca_cert.csr
  if [ $? -eq 0 ]; then
    echo "attest_ca_cert certificate request done.  3652 days"
  else
   echo "attest_ca_cert certificate request failed."
    exit
  fi  
}

create_attest_cert_CSR_func() {
  echo "Running create_attest_cert_CSR_func"
  ###############################################################################################
  # Create server private key and certificate signing request.
  # 0102030401020304 is the customer PRODUCT ID which also must be blown into CUSTOMER_ID fuse
  ###############################################################################################
  openssl req -new -nodes -newkey rsa:2048 -sha256 -days 730 -subj /C=IL/ST=NI/L="Location"/OU="R&D"/OU="01 010203041a1b1c1d PROD_ID"/O=TELIT/CN="attest_cert" -keyout attest_private.key -out attest_cert.csr
  if [ $? -eq 0 ]; then
    echo "attest certificate request done. 3652 days"
  else
    echo "attest certificate request failed."
    exit
  fi  
}

convertToDer_func() {
  ###############################################################################################
  # Strip from the certificate it's info header
  ###############################################################################################
  #echo "Striping the info header of the certificate." 
  #sed -i '1,/-----BEGIN CERTIFICATE-----/c-----BEGIN CERTIFICATE-----' attest_ca_cert.crt
  #if [ $? -eq 0 ]; then
  #  echo "Removed the header of attest_ca_cert certificate."
  #else
  #  echo "Removing the header of attest_ca_cert certificate failed."
  #  exit
  #fi
  
  #echo "Striping the info header of the certificate." 
  #sed -i '1,/-----BEGIN CERTIFICATE-----/c-----BEGIN CERTIFICATE-----' attest_cert.crt
  #if [ $? -eq 0 ]; then
  #  echo "Removed the header of attest_cert certificate."
  #else
  #  echo "Removing the header of attest_cert certificate failed."
  #  exit
  #fi
  echo "Converting certificates from pem to der format."
  ###############################################################################################
  # Convert certificates from pem to der format.
  ############################################################################################### 
#  openssl x509 -outform der -in ca_cert.crt -out ca_cert.der
 # if [ $? -eq 0 ]; then
    echo "Convert ca_cert.crt from pem to der done... before nothing to see."
 # else
 #   echo "Convert ca_cert.crt from pem to der failed."
  #  exit
  #fi
  
  openssl x509 -outform der -in attest_ca_cert.crt -out attest_ca_cert.der
  if [ $? -eq 0 ]; then
    echo "Convert attest_ca_cert.crt from pem to der done."
  else
    echo "Convert attest_ca_cert.crt from pem to der failed."
    exit
  fi

  openssl x509 -outform der -in attest_cert.crt -out attest_cert.der
  if [ $? -eq 0 ]; then
    echo "Convert attest_cert.crt from pem to der done."
  else
    echo "Convert attest_cert.crt from pem to der failed."
    exit
  fi  
}

extract_public_key_func() {

 # openssl rsa -in ca_private.key -outform PEM -pubout -out ca_public.key
  #if [ $? -eq 0 ]; then
    echo "Converting ca_private.key to public done... before nothing to see"
  #else
  #  echo "Converting ca_private.key to public failed."
  #  exit
  #fi
  
  openssl rsa -in attest_ca_private.key -outform PEM -pubout -out attest_ca_public.key
  if [ $? -eq 0 ]; then
    echo "Convert attest_ca_private.key to public done."
  else
    echo "Convert attest_ca_private.key to public failed."
    exit
  fi

  openssl rsa -in attest_private.key -outform PEM -pubout -out attest_public.key
  if [ $? -eq 0 ]; then
    echo "Convert attest_private.key to public done."
  else
    echo "Convert attest_private.key to public failed."
    exit
  fi

}

makePkg_func() {
  echo "Running makePkg" 
 
  ###############################################################################################
  # Prepare the pack to be copied to the server
  ###############################################################################################
  echo "Prepare the files pack"

  if [ -d ready_files_pack ]; then
    echo "ready_files_pack directory already exist. Saving it and creating a new one."
    mv -f ready_files_pack ready_files_pack.prev
  else
    mkdir ready_files_pack
  fi

# Copy files only... keep originals. 
  cp ca_private.key        ready_files_pack/ca_private.key
  cp ca_public.key         ready_files_pack/ca_public.key
  cp ca_cert.crt           ready_files_pack/ca_cert.pem
  cp ca_cert.der           ready_files_pack/ca_cert.der

  mv attest_ca_private.key ready_files_pack/attest_ca_private.key
  mv attest_ca_public.key  ready_files_pack/attest_ca_public.key
  mv attest_ca_cert.crt    ready_files_pack/attest_ca_cert.pem
  mv attest_ca_cert.der    ready_files_pack/attest_ca_cert.der

  mv attest_private.key    ready_files_pack/attest_private.key
  mv attest_public.key     ready_files_pack/attest_public.key
  mv attest_cert.crt       ready_files_pack/attest_cert.pem
  mv attest_cert.der       ready_files_pack/attest_cert.der
  rm ca_cert_hash_file
  mv ca_cert_hash_file_str ready_files_pack/ca_cert_hash_file_str
  
  rm *.csr
  rm *.pem
}

create_hash() {
   openssl dgst -sha256 -binary ca_cert.der > ca_cert_hash_file
   cat ca_cert_hash_file | hexdump -ve '/1 "%02x"' > ca_cert_hash_file_str.txt
}

sign_attest_ca_cert_CSR_func() {
  echo "Running sign_attest_ca_cert_CSR_func"
  ###############################################################################################
  # Sign the certificate signing request with CA certificate
  ###############################################################################################
  # TODO: Make it answer yes and yes automatically
  openssl ca -policy policy_anything -md SHA256 -cert ca_cert.crt -keyfile ca_private.key -outdir . -out attest_ca_cert.crt -infiles attest_ca_cert.csr
  if [ $? -eq 0 ]; then
    echo "Server request sign done."
  else
    echo "Server request sign failed."
    exit
  fi 
}

sign_attest_cert_CSR_func() {
  echo "Running sign_attest_cert_CSR_func"
  ###############################################################################################
  # Sign the certificate signing request with CA certificate
  ###############################################################################################
  # TODO: Make it answer yes and yes automatically
  openssl ca -policy policy_anything -md SHA256 -cert attest_ca_cert.crt -keyfile attest_ca_private.key -outdir . -out attest_cert.crt -infiles attest_cert.csr
  if [ $? -eq 0 ]; then
    echo "attest_cert request sign done."
  else
    echo "attest_cert request sign failed."
    exit
  fi 
}

cleanAll_func() {
  echo "Running cleanAll"
  rm -rf demoCA
  rm -rf ready_files_pack
  rm -f *.key
  rm -f *.key.public
  rm -f *.crt
  rm -f *.csr
  rm -f *.pem
  rm -f *.der
}

all_func() {
  echo "Running all the steps."
  createCApair_func
  create_attest_ca_cert_CSR_func
  create_attest_cert_CSR_func
  sign_attest_ca_cert_CSR_func
  sign_attest_cert_CSR_func
  convertToDer_func
  extract_public_key_func
  create_hash
  makePkg_func
}

init_func() {
  echo "Running basic init"
  
  if [ ! -d demoCA ]; then
    mkdir demoCA
    touch demoCA/index.txt
    echo 01 > demoCA/serial
  else
    echo "demoCA directory already exist."
  fi
}

if [ $# -eq 0 ]; then
  print_usage
  exit
fi

while [ $# -ne 0 ]
do
    arg="$1"
    case "$arg" in
        all)
            all_func
            exit
            ;;    
        clean)
            cleanAll_func
            exit
            ;;
        *)
            echo "arg = " $arg " Unrecognized."
            exit
            ;;
    esac
    shift
done
