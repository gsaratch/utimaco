
#include "hsm.hpp"
using namespace cxi;
/******************************************************************************
 *
 * cxi_demo_list_keys
 *
 ******************************************************************************/
void cxi_demo_list_keys(Cxi *cxi)
{    
	try{
	  printf("\nlisting all keys in group: %s ...\n", Group);
	  
	  PropertyList keyTemplate,temp,key1;
	  keyTemplate.setGroup("GAC_SC");
	  Key rsakey;//=keyList[0];

	  KeyList keyList = cxi->key_list(keyTemplate);
	  Property prop;
	  Blob keyBlob;


	  keyList.sort();  
	      
	  printf("\n");
	  printf("%-3s %-5s %-4s %-4s %-24s %-24s %s\n", "idx", "algo", "size", "type", "group", "name", "spec");
	  printf("--------------------------------------------------------------------------------\n");
	  
	  for (int i=0; i<keyList.size(); i++)
	  {      
	    printf("%-3d %-5s %-4d %-4x %-24s %-24s %-d\n", i,                
	                                                    AlgoNames[keyList[i].getAlgo() % DIM(AlgoNames)],
	                                                    keyList[i].getSize(),
	                                                    keyList[i].getType(),
	                                                    keyList[i].getGroup(),
	                                                    keyList[i].getName(),
	                                                    keyList[i].getSpecifier());  
	   
	    
	  }
	  printf("OK\n");
	}
	catch(cxi::Exception e)
		{
			printf("CXI Exception occured in displaying keys %s\n");

		}
}


int loginToServer(char *username,char *password,char *serverIP)
{
	Cxi *cxi=NULL;
	try {
		cxi = new Cxi(serverIP, 20000, 50000);
		cxi->logon_pass(username, password, true);
		return 1;
	}
	catch(cxi::Exception e)
	{
		return 0;
	}
}

void generateKeys(char *username,char *password,char *serverIP)
{

	PropertyList keyTemplate_a,keyTemplate_b,keyTemplate_c;
	Key rsakey_a,rsakey_b,rsakey_c;
	
	Cxi *cxi=NULL;
	try
	{
			cxi = new Cxi(serverIP, 20000, 50000);
			cxi->logon_pass(username, password, true);
			
			if(cxi->get_auth_state()!=0){
			
			
			keyTemplate_a.clear();
			keyTemplate_a.setAlgo(CXI_KEY_ALGO_RSA);    
			keyTemplate_a.setSize(2048);
			keyTemplate_a.setGroup("vist-dev-keys");
			keyTemplate_a.setName("rsakey_a");  
			rsakey_a = cxi->key_generate(CXI_FLAG_KEY_OVERWRITE, keyTemplate_a);  

			keyTemplate_b.clear();
			keyTemplate_b.setAlgo(CXI_KEY_ALGO_RSA);    
			keyTemplate_b.setSize(2048);
			keyTemplate_b.setGroup("vist-dev-keys");
			keyTemplate_b.setName("rsakey_b");  
			rsakey_b = cxi->key_generate(CXI_FLAG_KEY_OVERWRITE, keyTemplate_b);  

			keyTemplate_c.clear();
			keyTemplate_c.setAlgo(CXI_KEY_ALGO_RSA);    
			keyTemplate_c.setSize(2048);
			keyTemplate_c.setGroup("vist-dev-keys");
			keyTemplate_c.setName("rsakey_c");  
			rsakey_c = cxi->key_generate(CXI_FLAG_KEY_OVERWRITE, keyTemplate_c);

		}


	}
	catch(cxi::Exception e){
		printf("\nException occured while generating the keys");
	}
}

void generateKeysBatch(char *username,char *password,char *serverIP,char *keyFile)
{

	PropertyList keyTemplate_a,keyTemplate_b,keyTemplate_c;
	Key rsakey_a,rsakey_b,rsakey_c;
	char fileName[40];
	FILE *fp=NULL;
	Cxi *cxi=NULL;
	fp=fopen(keyFile,"r");
	
	if(fp){
			try
			{
			cxi = new Cxi(serverIP, 20000, 50000);
			cxi->logon_pass(username, password, true);
			
			if(cxi->get_auth_state()!=0){
			
				while(fgets(fileName,40,fp)!=NULL){
					fileName[strlen(fileName)-1]='\0';
					//printf("\n%s",fileName);
					keyTemplate_a.clear();
					keyTemplate_a.setAlgo(CXI_KEY_ALGO_RSA);    
					keyTemplate_a.setSize(2048);
					keyTemplate_a.setGroup("vist-dev-keys");
					keyTemplate_a.setName(fileName);  
					cxi->key_generate(CXI_FLAG_KEY_OVERWRITE, keyTemplate_a);		
				}
			
		}


	}
	catch(cxi::Exception e){
		printf("\nException occured while generating the keys");
	}
}
}
void deleteKeysBatch(char *username,char *password,char *serverIP,char *keyFile)
{

	PropertyList keyTemplate_a,keyTemplate_b,keyTemplate_c;
	Key rsakey_a,rsakey_b,rsakey_c;
	char fileName[40];
	FILE *fp=NULL;
	Cxi *cxi=NULL;
	fp=fopen(keyFile,"r");
	
	if(fp){
			try
			{
			cxi = new Cxi(serverIP, 20000, 50000);
			cxi->logon_pass(username, password, true);
			
			if(cxi->get_auth_state()!=0){
			
				while(fgets(fileName,40,fp)!=NULL){
					fileName[strlen(fileName)-1]='\0';
					//printf("\n%s",fileName);
					keyTemplate_a.clear();
					keyTemplate_a.setAlgo(CXI_KEY_ALGO_RSA);    
					keyTemplate_a.setSize(2048);
					keyTemplate_a.setGroup("vist-dev-keys");
					keyTemplate_a.setName(fileName);  
					cxi->key_delete(keyTemplate_a);		
				}
			
		}


	}
	catch(cxi::Exception e){
		printf("\nException occured while generating the keys");
	}
}	
	
}

void displayKeys(char *username,char *password,char *serverIP)
{

	unsigned char frontHeader[32]={0x30, 0x82, 0x01, 0x22, 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 
		0x82, 0x01, 0x0F, 0x00, 0x30, 0x82, 0x01, 0x0A, 0x02, 0x82, 0x01, 0x01};
	unsigned char middleHeader[2]={0x02,0x03};

	unsigned char k_a[294],length=0;
	Blob rsaKeyBlob_a,rsaKeyBlob_b,rsaKeyBlob_c;
	PropertyList keyTemplate_a,keyTemplate_b,keyTemplate_c;
	Key rsakey_a,rsakey_b,rsakey_c;
	Cxi *cxi=NULL;
	int i=0;
	FILE *fp=NULL;
	try
	{
			cxi = new Cxi(serverIP, 20000, 50000);
			cxi->logon_pass(username, password, true);
			
			if(cxi->get_auth_state()!=0){

			keyTemplate_a.clear();
			keyTemplate_a.setAlgo(CXI_KEY_ALGO_RSA);    
			keyTemplate_a.setSize(2048);
			keyTemplate_a.setGroup("vist-dev-keys");
			keyTemplate_a.setName("rsakey_a");  

			rsakey_a=cxi->key_open(0,keyTemplate_a);
			rsaKeyBlob_a = cxi->key_export(CXI_KEY_BLOB_SIMPLE|CXI_KEY_TYPE_PUBLIC, rsakey_a, NULL_REF(Key), CXI_MECH_PAD_PKCS1);        
			rsaKeyBlob_a.getKeyComp("MO").xtrace("Modulus_a");
			rsaKeyBlob_a.getKeyComp("PE").xtrace("Public Exponent_a");

			ByteArray e,n;
			n=rsaKeyBlob_a.getKeyComp("MO");
			e=rsaKeyBlob_a.getKeyComp("PE");

			printf("\nLength of n=%d, e=%d\n",n.length(),e.length());

			n.xtrace("modulus");
			e.xtrace("exponent");



			i=0;

			for(int j=0;j<32;j++){
				k_a[i++]=frontHeader[j];
				//printf("%0x ",frontHeader[j]);
			}

			k_a[i++]=0x00;
			for(int j=0;j<256;j++){
				k_a[i++]=(unsigned char)n[j];
			}

			for(int j=0;j<2;j++){
				k_a[i++]=middleHeader[j];
			}
			for(int j=0;j<3;j++){
				k_a[i++]=e[j];
			}

			printf("The total key is:\n");

			for(int j=0;j<294;j++){
				if(j%16==0)
					printf("\n");
				printf("%02x ",k_a[j]);
				
			}
			fp=fopen("temp_a","wb");
			if(fp!=NULL)
			{
				fwrite(k_a,1,294,fp);
			}
			fclose(fp);
			

			}
	}
	catch(cxi::Exception e){
		printf("\nException occured in displaying keys");	
	}
}


void constructTbsDer(Cxi *cxi,struct TLV *tlv,char *keyName)
{
    try{
	  
	  PropertyList keyTemplate;
	  keyTemplate.setGroup("vist-dev-keys");
	  Key rsakey;
	  Blob rsaKeyBlob;
	  ByteArray e,n;  
	  unsigned char modulus[257],exponent[3],*data=NULL;
      struct TLV temp[10];
	  
	  int dataLength,i,modulus_length=257,exponent_length=3;

	  KeyList keyList = cxi->key_list(keyTemplate);
	  keyList.sort();  
	  

	  for (i=0; i<keyList.size(); i++)  {
	  	if(strcmp(keyList[i].getName(),keyName)==0) {
	  		//printf("\nKey Found: %s",keyName);
	  		break;	
	  	}      
	    
	  }
	  if(i!=keyList.size()){
		  	rsakey=cxi->key_open(0,keyList[i]);
	  		rsaKeyBlob = cxi->key_export(CXI_KEY_BLOB_SIMPLE|CXI_KEY_TYPE_PUBLIC, rsakey, NULL_REF(Key), CXI_MECH_PAD_PKCS1);        
	
		
			n=rsaKeyBlob.getKeyComp("MO");
			e=rsaKeyBlob.getKeyComp("PE");

			//printf("\nLength of n=%d, e=%d\n",n.length(),e.length());

			//n.xtrace("modulus");
			//e.xtrace("exponent");
			modulus[0]=0x00;
			for(int j=0;j<n.length();j++){
				modulus[j+1]=n[j];
			}
			for(int j=0;j<e.length();j++){
				exponent[j]=e[j];
			}
			version(&temp[0]);      //version(&version_tlv);
			serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
			signatureAlgorithm(&temp[2]); //signature(&signature_tlv);
			issuerSubjectInfo(&temp[3],true); //issuerSubjectInfo(&issuer_tlv);
			validity(&temp[4]); //validaity(&validity_tlv);
			issuerSubjectInfo(&temp[5],false); //issuerSubjectInfo(&subject_tlv);
			subjectPublicKeyInfo(&temp[6],modulus,modulus_length,exponent,exponent_length); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
			//extensions(&temp[7]); //extensions(&extensions_tlv);
			
			dataLength=0;
			for(int j=0;j<7;j++){
				dataLength+=temp[j].length+temp[j].valueLength+1;
				//printf("\ndataLength=%d",dataLength);
			}
			data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
			if(data==NULL){
				printf("\nMemory allocation failed");
			}
			int t=0;
			for(j=0;j<7;j++){
				//printf("\nt=%d dataLength=%d",t,dataLength);

				tlvToStream(data+t,temp[j].length+temp[j].valueLength+1,&temp[j]);
				t+=temp[j].length+temp[j].valueLength+1;
			}
			encode(data,dataLength,0x30,tlv);
			free(data);
			
	  }
	  
	}
	catch(cxi::Exception e)
		{
			printf("CXI Exception occured in displaying keys %s\n");

		}
}


void constructTbsDerWithExtensions(Cxi *cxi,struct TLV *tlv,char *keyName)
{
    try{
	  
	  PropertyList keyTemplate;
	  keyTemplate.setGroup("vist-dev-keys");
	  Key rsakey;
	  Blob rsaKeyBlob;
	  ByteArray e,n;  
	  unsigned char modulus[257],exponent[3],*data=NULL;
      struct TLV temp[10];
	  
	  int dataLength,i,modulus_length=257,exponent_length=3;

	  KeyList keyList = cxi->key_list(keyTemplate);
	  keyList.sort();  
	  

	  for (i=0; i<keyList.size(); i++)  {
	  	if(strcmp(keyList[i].getName(),keyName)==0) {
	  		//printf("\nKey Found: %s",keyName);
	  		break;	
	  	}      
	    
	  }
	  if(i!=keyList.size()){
		  	rsakey=cxi->key_open(0,keyList[i]);
	  		rsaKeyBlob = cxi->key_export(CXI_KEY_BLOB_SIMPLE|CXI_KEY_TYPE_PUBLIC, rsakey, NULL_REF(Key), CXI_MECH_PAD_PKCS1);        
	
		
			n=rsaKeyBlob.getKeyComp("MO");
			e=rsaKeyBlob.getKeyComp("PE");

			//printf("\nLength of n=%d, e=%d\n",n.length(),e.length());

			//n.xtrace("modulus");
			//e.xtrace("exponent");
			modulus[0]=0x00;
			for(int j=0;j<n.length();j++){
				modulus[j+1]=n[j];
			}
			for(int j=0;j<e.length();j++){
				exponent[j]=e[j];
			}
			version(&temp[0]);      //version(&version_tlv);
			serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
			signatureAlgorithm(&temp[2]); //signature(&signature_tlv);
			issuerSubjectInfo(&temp[3],true); //issuerSubjectInfo(&issuer_tlv);
			validity(&temp[4]); //validaity(&validity_tlv);
			issuerSubjectInfo(&temp[5],false); //issuerSubjectInfo(&subject_tlv);
			subjectPublicKeyInfo(&temp[6],modulus,modulus_length,exponent,exponent_length); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
			extensions(&temp[7],true); //extensions(&extensions_tlv);
			
			dataLength=0;
			for(int j=0;j<8;j++){
				dataLength+=temp[j].length+temp[j].valueLength+1;
				//printf("\ndataLength=%d",dataLength);
			}
			data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
			if(data==NULL){
				printf("\nMemory allocation failed");
			}
			int t=0;
			for(j=0;j<8;j++){
				//printf("\nt=%d dataLength=%d",t,dataLength);

				tlvToStream(data+t,temp[j].length+temp[j].valueLength+1,&temp[j]);
				t+=temp[j].length+temp[j].valueLength+1;
			}
			encode(data,dataLength,0x30,tlv);
			free(data);
			
	  }
	  
	}
	catch(cxi::Exception e)
		{
			printf("CXI Exception occured in displaying keys %s\n");

		}
}
/*============================================================================
** Function Name:    computeDigest

** Description:      This Function computes the digest of a given file.
** Inputs:          fileName - The name of the file to which digest needs to be compiuted.
					     dig - The digest that is computed
				      digLen - Size of the digest

** Outputs:          1 - On successful computation of the digest to the file
					 0 - On Failure	
**==========================================================================*/
int computeDigestSHA1(char *msg,int msgLen,unsigned char *dgst,unsigned int *dgstLen)
{

	EVP_MD_CTX ctx;
	char buf[BUF_SIZE];
	int bufLen,ret=0,bytesRead;
	
	EVP_MD_CTX_init(&ctx);

	if(1 != EVP_DigestInit_ex(&ctx, EVP_sha1(), NULL)) goto err;
	
	if(1 != EVP_DigestUpdate(&ctx, msg,msgLen)) goto err;
		
	if(1 != EVP_DigestFinal_ex(&ctx, dgst,dgstLen)) goto err;
	
		
	ret=1;
err:
	if(ret!=1) {
		printf("\nError occuered in computing digest");
	}
	return ret;

}


void constructTbsDerFromConfigForSS(Cxi *cxi,struct TLV *tlv,char *keyToSign,char *keyToBeSigned,char *keyToSignConfig,char *keyToBeSignedConfig,bool caFlag)
{
	struct TLV publicKeyInfo,publicKeyInfo_a;
     
    try{
	  
	  PropertyList keyTemplate;
	  keyTemplate.setGroup("GAC_SC");
	  Key rsakey,rsakey_a;
	  Blob rsaKeyBlob,rsaKeyBlob_a;
	  ByteArray e,n,e_a,n_a;  
	  unsigned char modulus[257],exponent[3],modulus_a[257],exponent_a[3],*data=NULL;
      struct TLV temp[10];
	  int numberOfFields=8;

      /*if(caFlag)
      	numberOfFields=8;
	  */
	  int dataLength,i,j,dgstLen,dgstLen_a,modulus_length=257,exponent_length=3;

	  KeyList keyList = cxi->key_list(keyTemplate);
	  keyList.sort();  
	  unsigned char dgst[20],dgst_a[20];

	  for (i=0; i<keyList.size(); i++)  {
	  	if(strcmp(keyList[i].getName(),keyToBeSigned)==0) {
	  		break;	
	  	}      
	    
	  }
	   for (j=0; j<keyList.size(); j++)  {
	  	if(strcmp(keyList[j].getName(),keyToSign)==0) {
	  		break;	
	  	}      
	    
	  }
	  
	  if((i!=keyList.size())&&(j!=keyList.size())){
	  	  	rsakey=cxi->key_open(0,keyList[i]);
	  		rsaKeyBlob = cxi->key_export(CXI_KEY_BLOB_SIMPLE|CXI_KEY_TYPE_PUBLIC, rsakey, NULL_REF(Key), CXI_MECH_PAD_PKCS1);        
	
		
			n=rsaKeyBlob.getKeyComp("MO");
			e=rsaKeyBlob.getKeyComp("PE");

			n.xtrace("MO");
			e.xtrace("PE");
			
			modulus[0]=0x00;
			for(int k=0;k<n.length();k++){
				modulus[k+1]=n[k];
			}
			for(int k=0;k<e.length();k++){
				exponent[k]=e[k];
			}
			/*
			for(int k=253;k<e.length();k++){
				exponent[k-253]=e[k];
			}*/
			printf("\nexponent::");
			for(int i=0;i<3;i++)
				printf("%02x\t",exponent[i]);
			rsakey_a=cxi->key_open(0,keyList[j]);
	  		rsaKeyBlob_a= cxi->key_export(CXI_KEY_BLOB_SIMPLE|CXI_KEY_TYPE_PUBLIC, rsakey_a, NULL_REF(Key), CXI_MECH_PAD_PKCS1);        
	
		
			n_a=rsaKeyBlob_a.getKeyComp("MO");
			e_a=rsaKeyBlob_a.getKeyComp("PE");

			
			n_a.xtrace("MO");
			e_a.xtrace("PE");
			
			modulus_a[0]=0x00;
			for(int k=0;k<n.length();k++){
				modulus_a[k+1]=n_a[k];
			}
			/*
			for(int k=253;k<e.length();k++){
				exponent_a[k-253]=e_a[k];
			}
			*/
			for(int k=0;k<e.length();k++){
				exponent_a[k]=e_a[k];
			}
	
			printf("\nexponent::");
			for(int i=0;i<3;i++)
				printf("%02x\t",exponent_a[i]);
			
			version(&temp[0]);      //version(&version_tlv);
			serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
			signatureAlgorithmPSS(&temp[2]); //signature(&signature_tlv);
			issuerSubjectInfoFromConfigForSS(&temp[3],true,keyToSignConfig); //issuerSubjectInfo(&issuer_tlv);
			validity(&temp[4]); //validaity(&validity_tlv);
			issuerSubjectInfoFromConfigForSS(&temp[5],false,keyToBeSignedConfig); //issuerSubjectInfo(&subject_tlv);
			publicKeyInfo=subjectPublicKeyInfo(&temp[6],modulus,modulus_length,exponent,exponent_length); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
			
			struct TLV temp_a;
			publicKeyInfo_a=subjectPublicKeyInfo(&temp_a,modulus_a,modulus_length,exponent_a,exponent_length); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
			
			dataLength=publicKeyInfo.length+publicKeyInfo.valueLength+1;
			data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
			tlvToStream(data,dataLength,&publicKeyInfo);
			computeDigestSHA1(data,dataLength,dgst,&dgstLen);
			free(data);
			dataLength=publicKeyInfo_a.length+publicKeyInfo_a.valueLength+1;
			data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
			tlvToStream(data,dataLength,&publicKeyInfo_a);
			computeDigestSHA1(data,dataLength,dgst_a,&dgstLen_a);
			free(data);
			
			my_extensions(&temp[7],caFlag,dgst,dgstLen,dgst_a,dgstLen_a); //extensions(&extensions_tlv);
			
			dataLength=0;
			for(int j=0;j<numberOfFields;j++){
				dataLength+=temp[j].length+temp[j].valueLength+1;
				//printf("\ndataLength=%d",dataLength);
			}
			data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
			if(data==NULL){
				printf("\nMemory allocation failed");
			}
			int t=0;
			for(j=0;j<numberOfFields;j++){
				//printf("\nt=%d dataLength=%d",t,dataLength);

				tlvToStream(data+t,temp[j].length+temp[j].valueLength+1,&temp[j]);
				t+=temp[j].length+temp[j].valueLength+1;
			}
			encode(data,dataLength,0x30,tlv);
			free(data);
			
	  }
	  
	}
	catch(cxi::Exception e)
		{
			printf("CXI Exception occured in displaying keys %s\n");

		}

}

void constructTbsDerFromConfigCSRForSS(Cxi *cxi,struct TLV *tlv,char *keyName,char *keyToSignConfig,char *keyToBeSignedConfig,char *modulusFileName,bool caFlag)
{
    try{
	  
	  unsigned char modulus[257],exponent[3]={0x01,0x00,0x01},*data=NULL;
      struct TLV temp[10];
      int numberOfFields=8;
      /*if(caFlag)
      	numberOfFields=8;
	  */
	  int dataLength,i,modulus_length=257,exponent_length=3;

	  
	  		version(&temp[0]);      //version(&version_tlv);
			serialNumber(&temp[1]);  //serialNumber(&serialNumber_tlv);
			signatureAlgorithm(&temp[2]); //signature(&signature_tlv);
			issuerSubjectInfoFromConfigForSS(&temp[3],true,keyToSignConfig); //issuerSubjectInfo(&issuer_tlv);
			validity(&temp[4]); //validaity(&validity_tlv);
			issuerSubjectInfoFromConfigForSS(&temp[5],false,keyToBeSignedConfig); //issuerSubjectInfo(&subject_tlv);
			subjectPublicKeyInfoFromFile(&temp[6],modulusFileName,exponent,exponent_length); //subjectPublicKeyInfo(&subjectPublicKeyInfo_tlv);
			
			extensions(&temp[7],caFlag); //extensions(&extensions_tlv);
			
			dataLength=0;
			for(int j=0;j<numberOfFields;j++){
				dataLength+=temp[j].length+temp[j].valueLength+1;
				//printf("\ndataLength=%d",dataLength);
			}
			data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
			if(data==NULL){
				printf("\nMemory allocation failed");
			}
			int t=0;
			for(j=0;j<numberOfFields;j++){
				//printf("\nt=%d dataLength=%d",t,dataLength);

				tlvToStream(data+t,temp[j].length+temp[j].valueLength+1,&temp[j]);
				t+=temp[j].length+temp[j].valueLength+1;
			}
			encode(data,dataLength,0x30,tlv);
			free(data);
			
	  
	  
	}
	catch(cxi::Exception e)
		{
			printf("CXI Exception occured in displaying keys %s\n");

		}
}

int signFileUtimaco(Cxi *cxi, char *inFile, char *keyName, unsigned char **signature, int *signatureLength,char *outFile)
{
	
	PropertyList keyTemplate,key1; 
	KeyList keyList;
	Key rsakey;
	FILE *fp=NULL,*fp1=NULL;
	MechanismParameter mechParam;
	ByteArray sign;

	unsigned char dgst[BUF_SIZE],*sig;
	unsigned int dgstLen=0;
	char tempFile[BUF_SIZE],buf[BUF_SIZE];
	int i,sigLen=0,ret=0,bytesRead;
	bool result=false;
	ByteArray data=ByteArray(32);
	Hash hash;	
		
		printf("\nFileName:%s, keyName:%s",inFile,keyName);
	if(NULL==(fp1=fopen(inFile,"rb"))) {
		printf("\nError in opening the file: %s",inFile);
		goto err;
	}
	
	// create SHA-256 hash
	
	hash.init(CXI_MECH_HASH_ALGO_SHA256);
	while(0 != (bytesRead=fread(buf,1,BUF_SIZE,fp1))) {
	//printf("\nLength of the data read:%d:%s",strlen(buf),buf);	
	hash.update(buf,bytesRead);
	}
	
	hash.final();
	// dump hash value
	
	hash.xtrace("hash");
	
	
	keyTemplate.setGroup("vist-dev-keys");
	keyList=cxi->key_list(keyTemplate);
	for(i=0;i<keyList.size();i++)
	{
		if(strcmp(keyName,keyList[i].getName())==0){
			rsakey=cxi->key_open(0,keyList[i]);
			break;
		}
	}
	
	// sign data
	mechParam = MechanismParameter(CXI_MECH_PAD_PKCS1|CXI_MECH_HASH_ALGO_SHA256);
	sign = cxi->sign(rsakey, mechParam, hash);

	*signature=(unsigned char*)malloc(sizeof(unsigned char)*sign.length());
	*signatureLength=sign.length();

	for(i=0;i<*signatureLength;i++){
		*(*signature+i)=sign[i];
	}

	sign.xtrace("signature");
	sigLen=sign.length();
	sig=(unsigned char *)sign.get();
	if(NULL==outFile)
	{
		strcpy(tempFile,inFile);
		strcat(tempFile,".sig");
	}
	else
	{
		strcpy(tempFile,outFile);
	}
	if(NULL==(fp=fopen(tempFile,"wb"))) goto err;
	
	fwrite(sig,1,sigLen,fp);
	if(fp) fclose(fp);
	
	ret=1;

	
	err:
		if(1 != ret)
		{
			throw;
		}	
		
	return ret;
}



int signFileUtimacoPSS(Cxi *cxi, char *inFile, char *keyName, unsigned char **signature, int *signatureLength,char *outFile)
{
	
	PropertyList keyTemplate,key1; 
	KeyList keyList;
	Key rsakey;
	FILE *fp=NULL,*fp1=NULL;
	MechanismParameter mechParam;
	ByteArray sign;

	unsigned char dgst[BUF_SIZE],*sig;
	unsigned int dgstLen=0;
	char tempFile[BUF_SIZE],buf[BUF_SIZE];
	int i,sigLen=0,ret=0,bytesRead;
	bool result=false;
	ByteArray data=ByteArray(32);
	Hash hash;	
		
		printf("\nFileName:%s, keyName:%s",inFile,keyName);
	if(NULL==(fp1=fopen(inFile,"rb"))) {
		printf("\nError in opening the file: %s",inFile);
		goto err;
	}
	
	// create SHA-256 hash
	
	hash.init(CXI_MECH_HASH_ALGO_SHA256);
	while(0 != (bytesRead=fread(buf,1,BUF_SIZE,fp1))) {
	//printf("\nLength of the data read:%d:%s",strlen(buf),buf);	
	hash.update(buf,bytesRead);
	}
	
	hash.final();
	// dump hash value
	
	hash.xtrace("hash");
	
	
	keyTemplate.setGroup("vist-dev-keys");
	keyList=cxi->key_list(keyTemplate);
	for(i=0;i<keyList.size();i++)
	{
		if(strcmp(keyName,keyList[i].getName())==0){
			rsakey=cxi->key_open(0,keyList[i]);
			break;
		}
	}
	
	// sign data
	mechParam = MechanismParameter(CXI_MECH_PAD_PSS|CXI_MECH_HASH_ALGO_SHA256);
	sign = cxi->sign(rsakey, mechParam, hash);

	*signature=(unsigned char*)malloc(sizeof(unsigned char)*sign.length());
	*signatureLength=sign.length();

	for(i=0;i<*signatureLength;i++){
		*(*signature+i)=sign[i];
	}

	sign.xtrace("signature");
	sigLen=sign.length();
	sig=(unsigned char *)sign.get();
	if(NULL==outFile)
	{
		strcpy(tempFile,inFile);
		strcat(tempFile,".sig");
	}
	else
	{
		strcpy(tempFile,outFile);
	}
	if(NULL==(fp=fopen(tempFile,"wb"))) goto err;
	
	fwrite(sig,1,sigLen,fp);
	if(fp) fclose(fp);
	
	ret=1;

	
	err:
		if(1 != ret)
		{
			throw;
		}	
		
	return ret;
}

int signRawDataUtimaco(Cxi *cxi, struct TLV *tlv, char *keyName, unsigned char **signature, int *signatureLength)
{
	
	PropertyList keyTemplate,key1; 
	KeyList keyList;
	Key rsakey;
	MechanismParameter mechParam;
	ByteArray sign;

	unsigned char dgst[BUF_SIZE],*sig;
	unsigned int dgstLen=0;
	char tempFile[BUF_SIZE],buf[BUF_SIZE];
	int i,sigLen=0,ret=0,bytesRead;
	bool result=false;
	ByteArray data=ByteArray(32);
	Hash hash;	
	unsigned char *tbsData=NULL;
	int tbsDataLength;

	tbsDataLength=tlv->length+tlv->valueLength+1;
	tbsData=(unsigned char*)malloc(sizeof(unsigned char)*tbsDataLength);

	tlvToStream(tbsData,tbsDataLength,tlv);
	
	// create SHA-256 hash
	
	hash.init(CXI_MECH_HASH_ALGO_SHA256);
	hash.update(tbsData,tbsDataLength);
	hash.final();
	
	// dump hash value
	//hash.xtrace("hash");
	
	
	keyTemplate.setGroup("GAC_SC");
	keyList=cxi->key_list(keyTemplate);
	for(i=0;i<keyList.size();i++)
	{
		if(strcmp(keyName,keyList[i].getName())==0){
			rsakey=cxi->key_open(0,keyList[i]);
			break;
		}
	}
	
	// sign data
	mechParam = MechanismParameter(CXI_MECH_PAD_PSS|CXI_MECH_HASH_ALGO_SHA256); // changed to pss
	sign = cxi->sign(rsakey, mechParam, hash);

	*signature=(unsigned char*)malloc(sizeof(unsigned char)*sign.length());
	*signatureLength=sign.length();

	for(i=0;i<*signatureLength;i++){
		*(*signature+i)=sign[i];
	}

	//sign.xtrace("signature");

	
	ret=1;

	err:
		if(1 != ret)
		{
			throw;
		}	
		
	return ret;
}

void buildCertificate(Cxi *cxi,char *keyName,struct TLV *tlv){
	struct TLV algorithmIdentifier,signatureValue_tlv,tbsCert_tlv;
	
	unsigned char *signature=NULL,*signedSignature=NULL,*data=NULL;
	int signatureLength,dataLength,signedSignatureLength,i;



	constructTbsDer(cxi,&tbsCert_tlv,keyName);
	signatureAlgorithm(&algorithmIdentifier);

	signRawDataUtimaco(cxi,&tbsCert_tlv,keyName,&signature,&signatureLength);

	signedSignatureLength=signatureLength+1;
	signedSignature=(unsigned char*)malloc(sizeof(unsigned char)*signedSignatureLength);
	signedSignature[0]=0x00;
	memcpy(signedSignature+1,signature,signatureLength);

	encode(signedSignature,signedSignatureLength,0x03,&signatureValue_tlv);
	
	dataLength=tbsCert_tlv.length+tbsCert_tlv.valueLength+1+algorithmIdentifier.length+algorithmIdentifier.valueLength+1+signatureValue_tlv.length+signatureValue_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=tbsCert_tlv.length+tbsCert_tlv.valueLength+1;

	tlvToStream(data,i,&tbsCert_tlv);
	tlvToStream(data+i,algorithmIdentifier.length+algorithmIdentifier.valueLength+1,&algorithmIdentifier);
	i+=algorithmIdentifier.length+algorithmIdentifier.valueLength+1;
	tlvToStream(data+i,signatureValue_tlv.length+signatureValue_tlv.valueLength+1,&signatureValue_tlv);

	encode(data,dataLength,0x30,tlv);
	
}

void createCertificate(Cxi *cxi, char *keyToSign,char *keyToBeSigned){
	struct TLV algorithmIdentifier,signatureValue_tlv,tbsCert_tlv,tlv;
	
	unsigned char *signature=NULL,*signedSignature=NULL,*data=NULL;
	int signatureLength,dataLength,signedSignatureLength,i;

	char certName[30];
	FILE *fp=NULL;

	constructTbsDer(cxi,&tbsCert_tlv,keyToBeSigned);
	signatureAlgorithm(&algorithmIdentifier);

	signRawDataUtimaco(cxi,&tbsCert_tlv,keyToSign,&signature,&signatureLength);

	signedSignatureLength=signatureLength+1;
	signedSignature=(unsigned char*)malloc(sizeof(unsigned char)*signedSignatureLength);
	signedSignature[0]=0x00;
	memcpy(signedSignature+1,signature,signatureLength);

	encode(signedSignature,signedSignatureLength,0x03,&signatureValue_tlv);
	
	dataLength=tbsCert_tlv.length+tbsCert_tlv.valueLength+1+algorithmIdentifier.length+algorithmIdentifier.valueLength+1+signatureValue_tlv.length+signatureValue_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=tbsCert_tlv.length+tbsCert_tlv.valueLength+1;

	tlvToStream(data,i,&tbsCert_tlv);
	tlvToStream(data+i,algorithmIdentifier.length+algorithmIdentifier.valueLength+1,&algorithmIdentifier);
	i+=algorithmIdentifier.length+algorithmIdentifier.valueLength+1;
	tlvToStream(data+i,signatureValue_tlv.length+signatureValue_tlv.valueLength+1,&signatureValue_tlv);

	encode(data,dataLength,0x30,&tlv);

	free(data);
	dataLength=tlv.length+tlv.valueLength+1;
	data=(unsigned char*) malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&tlv);

	strcpy(certName,keyToBeSigned);
	strcat(certName,".der");

	printf("Cert Name:%s",certName);

	fp=fopen(certName,"wb");
	if(fp){
		fwrite(data,1,dataLength,fp);
	}
	if(fp)
		fclose(fp);
	free(data);

}

void createCertificateFromConfig(Cxi *cxi, char *keyToSign,char *keyToBeSigned,char *keyToSignConfig,char *keyToBeSignedConfig,bool caFlag){
	struct TLV algorithmIdentifier,algorithmIdentifierPSS,signatureValue_tlv,tbsCert_tlv,tlv;
	
	unsigned char *signature=NULL,*signedSignature=NULL,*data=NULL;
	int signatureLength,dataLength,signedSignatureLength,i;
	Hash hash;
	char certName[30];
	FILE *fp=NULL;

	//signatureAlgorithmPSS(&algorithmIdentifierPSS);
	//display(&algorithmIdentifierPSS);
	constructTbsDerFromConfigForSS(cxi,&tbsCert_tlv,keyToSign,keyToBeSigned,keyToSignConfig,keyToBeSignedConfig,caFlag);

	signatureAlgorithmPSS(&algorithmIdentifier); //changed to PSS
	signRawDataUtimaco(cxi,&tbsCert_tlv,keyToSign,&signature,&signatureLength);

	signedSignatureLength=signatureLength+1;
	signedSignature=(unsigned char*)malloc(sizeof(unsigned char)*signedSignatureLength);
	signedSignature[0]=0x00;
	memcpy(signedSignature+1,signature,signatureLength);

	encode(signedSignature,signedSignatureLength,0x03,&signatureValue_tlv);
	
	dataLength=tbsCert_tlv.length+tbsCert_tlv.valueLength+1+algorithmIdentifier.length+algorithmIdentifier.valueLength+1+signatureValue_tlv.length+signatureValue_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=tbsCert_tlv.length+tbsCert_tlv.valueLength+1;

	tlvToStream(data,i,&tbsCert_tlv);
	tlvToStream(data+i,algorithmIdentifier.length+algorithmIdentifier.valueLength+1,&algorithmIdentifier);
	i+=algorithmIdentifier.length+algorithmIdentifier.valueLength+1;
	tlvToStream(data+i,signatureValue_tlv.length+signatureValue_tlv.valueLength+1,&signatureValue_tlv);

	encode(data,dataLength,0x30,&tlv);

	free(data);
	dataLength=tlv.length+tlv.valueLength+1;
	data=(unsigned char*) malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&tlv);

	strcpy(certName,keyToBeSigned);
	strcat(certName,".der");

	printf("\nCert Name:%s",certName);

	fp=fopen(certName,"wb");
	if(fp){
		fwrite(data,1,dataLength,fp);
	}
	if(fp)
		fclose(fp);
	free(data);
}

void createCertificateFromConfigCSR(Cxi *cxi, char *keyToSign,char *keyToBeSigned,char *keyToSignConfig,char *keyToBeSignedConfig,char *modulusFileName, bool caFlag){
	struct TLV algorithmIdentifier,signatureValue_tlv,tbsCert_tlv,tlv;
	
	unsigned char *signature=NULL,*signedSignature=NULL,*data=NULL;
	int signatureLength,dataLength,signedSignatureLength,i;

	char certName[30];
	FILE *fp=NULL;

	
	constructTbsDerFromConfigCSRForSS(cxi,&tbsCert_tlv,keyToBeSigned,keyToSignConfig,keyToBeSignedConfig,modulusFileName,caFlag);
	signatureAlgorithm(&algorithmIdentifier);

	signRawDataUtimaco(cxi,&tbsCert_tlv,keyToSign,&signature,&signatureLength);

	signedSignatureLength=signatureLength+1;
	signedSignature=(unsigned char*)malloc(sizeof(unsigned char)*signedSignatureLength);
	signedSignature[0]=0x00;
	memcpy(signedSignature+1,signature,signatureLength);

	encode(signedSignature,signedSignatureLength,0x03,&signatureValue_tlv);
	
	dataLength=tbsCert_tlv.length+tbsCert_tlv.valueLength+1+algorithmIdentifier.length+algorithmIdentifier.valueLength+1+signatureValue_tlv.length+signatureValue_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=tbsCert_tlv.length+tbsCert_tlv.valueLength+1;

	tlvToStream(data,i,&tbsCert_tlv);
	tlvToStream(data+i,algorithmIdentifier.length+algorithmIdentifier.valueLength+1,&algorithmIdentifier);
	i+=algorithmIdentifier.length+algorithmIdentifier.valueLength+1;
	tlvToStream(data+i,signatureValue_tlv.length+signatureValue_tlv.valueLength+1,&signatureValue_tlv);

	encode(data,dataLength,0x30,&tlv);

	free(data);
	dataLength=tlv.length+tlv.valueLength+1;
	data=(unsigned char*) malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&tlv);

	strcpy(certName,keyToBeSigned);
	strcat(certName,".der");

	printf("\nCert Name:%s",certName);

	fp=fopen(certName,"wb");
	if(fp){
		fwrite(data,1,dataLength,fp);
	}
	if(fp)
		fclose(fp);
	free(data);

}



void createIntermediateCertificate(Cxi *cxi, char *keyToSign,char *keyToBeSigned){
	struct TLV algorithmIdentifier,signatureValue_tlv,tbsCert_tlv,tlv;
	
	unsigned char *signature=NULL,*signedSignature=NULL,*data=NULL;
	int signatureLength,dataLength,signedSignatureLength,i;

	char certName[30]="cert_";
	FILE *fp=NULL;

	constructTbsDerWithExtensions(cxi,&tbsCert_tlv,keyToBeSigned);
	signatureAlgorithm(&algorithmIdentifier);

	signRawDataUtimaco(cxi,&tbsCert_tlv,keyToSign,&signature,&signatureLength);

	signedSignatureLength=signatureLength+1;
	signedSignature=(unsigned char*)malloc(sizeof(unsigned char)*signedSignatureLength);
	signedSignature[0]=0x00;
	memcpy(signedSignature+1,signature,signatureLength);

	encode(signedSignature,signedSignatureLength,0x03,&signatureValue_tlv);
	
	dataLength=tbsCert_tlv.length+tbsCert_tlv.valueLength+1+algorithmIdentifier.length+algorithmIdentifier.valueLength+1+signatureValue_tlv.length+signatureValue_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=tbsCert_tlv.length+tbsCert_tlv.valueLength+1;

	tlvToStream(data,i,&tbsCert_tlv);
	tlvToStream(data+i,algorithmIdentifier.length+algorithmIdentifier.valueLength+1,&algorithmIdentifier);
	i+=algorithmIdentifier.length+algorithmIdentifier.valueLength+1;
	tlvToStream(data+i,signatureValue_tlv.length+signatureValue_tlv.valueLength+1,&signatureValue_tlv);

	encode(data,dataLength,0x30,&tlv);

	free(data);
	dataLength=tlv.length+tlv.valueLength+1;
	data=(unsigned char*) malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,dataLength,&tlv);

	strcat(certName,keyToBeSigned);
	strcat(certName,".der");

	printf("Cert Name:%s",certName);

	fp=fopen(certName,"wb");
	if(fp){
		fwrite(data,1,dataLength,fp);
	}
	if(fp)
		fclose(fp);
	free(data);

}

