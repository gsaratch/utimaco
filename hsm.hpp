#include<iostream>
#include<stdio.h>
#include<string.h>
#include<openssl/evp.h>
#include<openssl/rsa.h>

#include "asn1.h"
#include "CXI/include/cxi.h"
using namespace std;
using namespace cxi;


/******************************************************************************
 *
 * Version
 *
 ******************************************************************************/
#define CXI_DEMO_VERSION  "1.0.1"
#define CXI_DEMO_DATE     __DATE__

/******************************************************************************
 *
 * Macros
 *
 ******************************************************************************/
#define CLEANUP(e) { err = (e); goto cleanup; }
#define DIM(x)     (sizeof((x))/sizeof((x[0])))

#ifndef MIN
#define MIN(a,b)   ((a)<(b)?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b)   ((a)>(b)?(a):(b))
#endif

#define BUF_SIZE 100

/******************************************************************************
 *
 * Globals 
 *
 ******************************************************************************/
static char *AlgoNames[] = { (char*)"RAW", (char*)"DES", (char*)"AES", (char*)"RSA", (char*)"ECDSA", (char*)"DSA", (char*)"ECDH", (char*)"DH" };

static char *Group = NULL;
static char *IP="136.18.56.62";
static char *userName="vist-dev-so001";
static char *passWord="03101724";

void cxi_demo_list_keys(Cxi *cxi);
int loginToServer(char *username,char *password,char *serverIP);
void generateKeys(char *username,char *password,char *serverIP);

void displayKeys(char *username,char *password,char *serverIP);
void constructTbsDer(Cxi *cxi,struct TLV *tlv,char *keyName);
int signFileUtimaco(Cxi *cxi, char *inFile, char *keyName, unsigned char **sign, int *signLength,char *outFile);
int signRawDataUtimaco(Cxi *cxi, struct TLV *tlv, char *keyName, unsigned char **signature, int *signatureLength);
void buildCertificate(Cxi *cxi,char *keyName,struct TLV *tlv);
void createCertificate(Cxi *cxi, char *keyToSign,char *keyToBeSigned);

void createCertificateFromConfig(Cxi *cxi, char *keyToSign,char *keyToBeSigned,char *keyToSignConfig,char *keyToBeSignedConfig,bool caFlag);
//void constructTbsDerFromConfigForSS(Cxi *cxi,struct TLV *tlv,char *keyName,char *keyToSignConfig,char *keyToBeSignedConfig,bool caFlag);
//struct TLV constructTbsDerFromConfigForSS(Cxi *cxi,struct TLV *tlv,char *keyName,char *keyToSignConfig,char *keyToBeSignedConfig,bool caFlag);
void constructTbsDerFromConfigForSS(Cxi *cxi,struct TLV *tlv,char *keyToSign,char *keyToBeSigned,char *keyToSignConfig,char *keyToBeSignedConfig,bool caFlag);


void constructTbsDerWithExtensions(Cxi *cxi,struct TLV *tlv,char *keyName);
void createIntermediateCertificate(Cxi *cxi, char *keyToSign,char *keyToBeSigned);

void generateKeysBatch(char *username,char *password,char *serverIP,char *keyFile);
void deleteKeysBatch(char *username,char *password,char *serverIP,char *keyFile);

void constructTbsDerFromConfigCSRForSS(Cxi *cxi,struct TLV *tlv,char *keyName,char *keyToSignConfig,char *keyToBeSignedConfig,char *modulusFileName,bool caFlag);
void createCertificateFromConfigCSR(Cxi *cxi, char *keyToSign,char *keyToBeSigned,char *keyToSignConfig,char *keyToBeSignedConfig,char *modulusFileName, bool caFlag);


int computeDigestSHA1(char *msg,int msgLen,unsigned char *dgst,unsigned int *dgstLen);
