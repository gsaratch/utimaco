openssl verify -CAfile BMW_GEN45_HAB_CA.pem BMW_GEN45_HAB_SRK1.pem
openssl verify -CAfile BMW_GEN45_HAB_CA.pem BMW_GEN45_HAB_SRK2.pem
openssl verify -CAfile BMW_GEN45_HAB_CA.pem BMW_GEN45_HAB_SRK3.pem
openssl verify -CAfile BMW_GEN45_HAB_CA.pem BMW_GEN45_HAB_SRK4.pem

#openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK1.pem BMW_GEN45_HAB_CSF1.pem
#openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK1.pem BMW_GEN45_HAB_IMG1.pem

openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK2.pem BMW_GEN45_HAB_CSF2.pem
openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK2.pem BMW_GEN45_HAB_IMG2.pem

openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK3.pem BMW_GEN45_HAB_CSF3.pem
openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK3.pem BMW_GEN45_HAB_IMG3.pem

openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK4.pem BMW_GEN45_HAB_CSF4.pem
openssl verify -CAfile BMW_GEN45_HAB_CA.pem -untrusted BMW_GEN45_HAB_SRK4.pem BMW_GEN45_HAB_IMG4.pem
