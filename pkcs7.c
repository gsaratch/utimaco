#include <stdio.h>
#include <malloc.h>
#include "asn1.h"



int main(int argc, char **argv)
{
	struct TLV signedData,tlv,temp,temp1,temp2,contentType_tlv,messageDigest_tlv,signingTime_tlv,issuer_tlv,randNum_tlv,version_tlv,
		digestAlgorithm_tlv,encryptionAlgorithm_tlv,signature_tlv,data_tlv,signedData_tlv;
	unsigned char hash[32],sig[256],*data=NULL;
	int dataLength,i=0;
	FILE *fp=NULL;

	pkcs7_version(&version_tlv);

	issuerSubjectInfoFromConfigForSS(&issuer_tlv,true,argv[1]);
	readHashSignature(hash,sig,argv[1]);
	pkcs7_randomNumber(&randNum_tlv);

	dataLength=issuer_tlv.length+issuer_tlv.valueLength+1+randNum_tlv.length+randNum_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,issuer_tlv.length+issuer_tlv.valueLength+1,&issuer_tlv);
	tlvToStream(data+issuer_tlv.length+issuer_tlv.valueLength+1,randNum_tlv.length+randNum_tlv.valueLength+1,&randNum_tlv);

	encode(data,dataLength,0x30,&temp);

	free(data);
		
	pkcs7_digestAlgorithm(&digestAlgorithm_tlv);

		
	contentType(&contentType_tlv);
	signingTime(&signingTime_tlv);
	messageDigest(&messageDigest_tlv,hash);
	
	dataLength=contentType_tlv.length+contentType_tlv.valueLength+1+signingTime_tlv.length+signingTime_tlv.valueLength+1+
		messageDigest_tlv.length+messageDigest_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,contentType_tlv.length+contentType_tlv.valueLength+1,&contentType_tlv);
	tlvToStream(data+contentType_tlv.length+contentType_tlv.valueLength+1,signingTime_tlv.length+signingTime_tlv.valueLength+1,&signingTime_tlv);
	tlvToStream(data+contentType_tlv.length+contentType_tlv.valueLength+1+signingTime_tlv.length+signingTime_tlv.valueLength+1,
		messageDigest_tlv.length+messageDigest_tlv.valueLength+1,&messageDigest_tlv);

	encode(data,dataLength,0xA0,&temp1);
	free(data);

	pkcs7_encryptionAlgorithm(&encryptionAlgorithm_tlv);
	pkcs7_rsaSignature(&signature_tlv,sig);

	

	dataLength=version_tlv.length+version_tlv.valueLength+1+temp.length+temp.valueLength+1+digestAlgorithm_tlv.length+digestAlgorithm_tlv.valueLength+1+
		temp1.length+temp1.valueLength+1+encryptionAlgorithm_tlv.length+encryptionAlgorithm_tlv.valueLength+1+signature_tlv.length+signature_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	i=0;
	tlvToStream(data,version_tlv.length+version_tlv.valueLength+1,&version_tlv);
	i+=version_tlv.length+version_tlv.valueLength+1;
	tlvToStream(data+i,temp.length+temp.valueLength+1,&temp);
	i+=temp.length+temp.valueLength+1;
	tlvToStream(data+i,digestAlgorithm_tlv.length+digestAlgorithm_tlv.valueLength+1,&digestAlgorithm_tlv);
	i+=digestAlgorithm_tlv.length+digestAlgorithm_tlv.valueLength+1;
	tlvToStream(data+i,temp1.length+temp1.valueLength+1,&temp1);
	i+=temp1.length+temp1.valueLength+1;	
	tlvToStream(data+i,encryptionAlgorithm_tlv.length+encryptionAlgorithm_tlv.valueLength+1,&encryptionAlgorithm_tlv);
	i+=encryptionAlgorithm_tlv.length+encryptionAlgorithm_tlv.valueLength+1;
	tlvToStream(data+i,signature_tlv.length+signature_tlv.valueLength+1,&signature_tlv);

	encode(data,dataLength,0x30,&temp);
	
	
	free(data);
	
	dataLength=temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	tlvToStream(data,dataLength,&temp);
	encode(data,dataLength,0x31,&temp);
	free(data);

	
	pkcs7_data(&data_tlv);

	dataLength=data_tlv.length+data_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	
	tlvToStream(data,dataLength,&data_tlv);
	encode(data,dataLength,0x30,&temp1);
	
	free(data);

	dataLength=digestAlgorithm_tlv.length+digestAlgorithm_tlv.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	/*tlvToStream(data,dataLength,&digestAlgorithm_tlv);
	encode(data,dataLength,0x30,&temp2);
	free(data);

	dataLength=temp2.length+temp2.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);*/
	tlvToStream(data,dataLength,&digestAlgorithm_tlv);  //some changes done
	encode(data,dataLength,0x31,&temp2);

	

	
	
	dataLength=version_tlv.length+version_tlv.valueLength+1+temp2.length+temp2.valueLength+1+temp1.length+temp1.valueLength+1+temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	i=0;
	
	tlvToStream(data,version_tlv.length+version_tlv.valueLength+1,&version_tlv);
	i+=version_tlv.length+version_tlv.valueLength+1;
	tlvToStream(data+i,temp2.length+temp2.valueLength+1,&temp2);
	i+=temp2.length+temp2.valueLength+1;
	tlvToStream(data+i,temp1.length+temp1.valueLength+1,&temp1);
	i+=temp1.length+temp1.valueLength+1;
	tlvToStream(data+i,temp.length+temp.valueLength+1,&temp);

	encode(data,dataLength,0x30,&temp);
	free(data);

	dataLength=temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&temp);	
	encode(data,dataLength,0xA0,&temp);
	free(data);
	
	pkcs7_signedData(&signedData_tlv);

	dataLength=signedData_tlv.length+signedData_tlv.valueLength+1+temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);

	tlvToStream(data,signedData_tlv.length+signedData_tlv.valueLength+1,&signedData_tlv);
	tlvToStream(data+signedData_tlv.length+signedData_tlv.valueLength+1,temp.length+temp.valueLength+1,&temp);

	encode(data,dataLength,0x30,&temp);
	free(data);

	dataLength=temp.length+temp.valueLength+1;
	data=(unsigned char*)malloc(sizeof(unsigned char)*dataLength);
	tlvToStream(data,dataLength,&temp);
		
	fp=fopen("cms.der","wb");
	if(fp){
		fwrite(data,1,dataLength,fp);
	}
	if(fp)
		fclose(fp);	
	
	return 0;
}
