#!/bin/sh
if [ ! -e "pkcs7" ]
then 
	echo "removing pkcs7 file"
	rm pkcs7
fi

g++ -o pkcs7 pkcs7.c asn1.c -w -g -fpermissive -pthread -ldl 

./pkcs7 issuer
