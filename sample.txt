sarat@sarat:~/Desktop/utimaco$ ./run.sh 

listing all keys in group: (null) ...

idx algo  size type group                    name                     spec
--------------------------------------------------------------------------------
0   RSA   2048 3    vist-dev-keys            EC_DEMO_KEY              -1
1   RSA   2048 3    vist-dev-keys            RSA_DEMO_KEY             -1
2   RSA   2048 3    vist-dev-keys            VST-DEV-KEY1             -1
3   RSA   2048 3    vist-dev-keys            ca_key                   -1
4   RSA   2048 3    vist-dev-keys            ca_key1                  -1
5   RSA   2048 3    vist-dev-keys            rsakey_a                 -1
6   RSA   2048 3    vist-dev-keys            rsakey_b                 -1
7   RSA   2048 3    vist-dev-keys            rsakey_c                 -1
OK

1.Self-Sign
2.Sign
3.Exit
Enter your choice:1

Enter Key Name To/ToBe Signed:rsakey_a

           Enter Issuer Details             
countryName: aa

stateOrProvinceName: aa

localityName: aa

organizationalUnitName: aa

organizationName: aa

commonName: aa

           Enter Subject Details            
countryName: aa

stateOrProvinceName: aa

localityName: aa

organizationalUnitName: aa

organizationName: aa

commonName: aa
Cert Name:cert_rsakey_a.der
1.Self-Sign
2.Sign
3.Exit
Enter your choice:2

Enter Key Name To Sign(Root Key):rsakey_a

Enter Key Name To Be Signed:rsakey_b

           Enter Issuer Details             
countryName: aa

stateOrProvinceName: aa

localityName: aa

organizationalUnitName: aa

organizationName: aa

commonName: aa

           Enter Subject Details            
countryName: bb

stateOrProvinceName: bb

localityName: bb

organizationalUnitName: bb

organizationName: bb

commonName: bb
Cert Name:cert_rsakey_b.der
1.Self-Sign
2.Sign
3.Exit
Enter your choice:2

Enter Key Name To Sign(Root Key):rsakey_b

Enter Key Name To Be Signed:rsakey_c

           Enter Issuer Details             
countryName: bb

stateOrProvinceName: bb

localityName: bb

organizationalUnitName: bb

organizationName: bb

commonName: bb

           Enter Subject Details            
countryName: cc

stateOrProvinceName: cc

localityName: cc

organizationalUnitName: cc

organizationName: cc

commonName: cc
Cert Name:cert_rsakey_c.der
1.Self-Sign
2.Sign
3.Exit
Enter your choice:3

